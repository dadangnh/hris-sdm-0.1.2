<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308094216 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_cuti_pegawai (id UUID NOT NULL, jns_cuti_id INT NOT NULL, permohonan_cuti_id UUID DEFAULT NULL, pegawai_id UUID NOT NULL, nip9 VARCHAR(9) DEFAULT NULL, no_urut_cuti_peg INT DEFAULT NULL, no_srt_izin_cuti VARCHAR(50) DEFAULT NULL, tgl_srt_izin_cuti DATE DEFAULT NULL, tgl_awal_cuti_peg DATE DEFAULT NULL, tgl_akhir_cuti_peg DATE DEFAULT NULL, jumlah_cuti DOUBLE PRECISION DEFAULT NULL, sisa_cuti DOUBLE PRECISION DEFAULT NULL, tahun VARCHAR(4) NOT NULL, nama_buat UUID NOT NULL, tanggal_buat DATE NOT NULL, nomor_ticket VARCHAR(50) NOT NULL, batal INT DEFAULT NULL, alasan_pembatalan TEXT DEFAULT NULL, persalinan_ke INT DEFAULT NULL, no_sket_dokter VARCHAR(255) DEFAULT NULL, status_perawatan VARCHAR(1) DEFAULT NULL, rumah_sakit VARCHAR(255) DEFAULT NULL, tanggal_perawatan_mulai DATE DEFAULT NULL, tanggal_perawatan_selesai DATE DEFAULT NULL, keguguran VARCHAR(1) DEFAULT NULL, ket_dokter VARCHAR(1) DEFAULT NULL, status_alasan_penting VARCHAR(2) DEFAULT NULL, lama_cuti_batal DOUBLE PRECISION DEFAULT NULL, tanggal_mulai_tugas DATE DEFAULT NULL, tanggal_selesai_tugas DATE DEFAULT NULL, jumlah_cuti_ditangguhkan_lt INT DEFAULT NULL, jumlah_cuti_lt INT DEFAULT NULL, tahun_lt VARCHAR(4) DEFAULT NULL, tgl_berangkat_haji DATE DEFAULT NULL, tgl_kembali_haji DATE DEFAULT NULL, tipe_cuti_half INT DEFAULT NULL, kd_dati2 UUID DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5AB09CD561377D47 ON t_cuti_pegawai (jns_cuti_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5AB09CD5D5CC105 ON t_cuti_pegawai (permohonan_cuti_id)');
        $this->addSql('CREATE INDEX idx_cuti_pegawai ON t_cuti_pegawai (id, nomor_ticket, tgl_awal_cuti_peg, tgl_akhir_cuti_peg, pegawai_id)');
        $this->addSql('COMMENT ON COLUMN t_cuti_pegawai.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_cuti_pegawai.permohonan_cuti_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_cuti_pegawai.pegawai_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_cuti_pegawai.nama_buat IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_cuti_pegawai.kd_dati2 IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE t_cuti_pegawai ADD CONSTRAINT FK_5AB09CD561377D47 FOREIGN KEY (jns_cuti_id) REFERENCES t_jns_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE t_cuti_pegawai ADD CONSTRAINT FK_5AB09CD5D5CC105 FOREIGN KEY (permohonan_cuti_id) REFERENCES t_permohonan_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_cuti_pegawai');
    }
}
