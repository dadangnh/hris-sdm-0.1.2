<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220421042212 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE t_jns_cuti_tambahan_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE t_jns_cuti_tambahan (id INT NOT NULL, nama_cuti_tambahan VARCHAR(75) NOT NULL, jns_cuti_tambahan INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_jns_cuti_tambahan ON t_jns_cuti_tambahan (id, nama_cuti_tambahan, jns_cuti_tambahan)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE t_jns_cuti_tambahan_id_seq CASCADE');
        $this->addSql('DROP TABLE t_jns_cuti_tambahan');
    }
}
