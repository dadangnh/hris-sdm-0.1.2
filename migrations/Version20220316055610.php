<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220316055610 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE t_cuti_pegawai DROP CONSTRAINT fk_5ab09cd561377d47');
        $this->addSql('DROP INDEX uniq_5ab09cd561377d47');
        $this->addSql('ALTER TABLE t_cuti_pegawai DROP jns_cuti_id');
        $this->addSql('ALTER TABLE t_permohonan_cuti DROP CONSTRAINT fk_1cd8142161377d47');
        $this->addSql('DROP INDEX uniq_1cd8142161377d47');
        $this->addSql('ALTER TABLE t_permohonan_cuti DROP jns_cuti_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD jns_cuti_id INT NOT NULL');
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD CONSTRAINT fk_1cd8142161377d47 FOREIGN KEY (jns_cuti_id) REFERENCES t_jns_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_1cd8142161377d47 ON t_permohonan_cuti (jns_cuti_id)');
        $this->addSql('ALTER TABLE t_cuti_pegawai ADD jns_cuti_id INT NOT NULL');
        $this->addSql('ALTER TABLE t_cuti_pegawai ADD CONSTRAINT fk_5ab09cd561377d47 FOREIGN KEY (jns_cuti_id) REFERENCES t_jns_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_5ab09cd561377d47 ON t_cuti_pegawai (jns_cuti_id)');
    }
}
