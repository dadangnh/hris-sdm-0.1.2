<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220517033618 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_permohonan_cuti_tambahan (id UUID NOT NULL, jenis_cuti_tambahan VARCHAR(50) NOT NULL, tanggal_mulai DATE DEFAULT NULL, tanggal_selesai DATE DEFAULT NULL, lama_cuti_tambahan DOUBLE PRECISION DEFAULT NULL, alasan_cuti_tambahan TEXT DEFAULT NULL, status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_permohonan_cuti_tambahan ON t_permohonan_cuti_tambahan (id, jenis_cuti_tambahan, tanggal_mulai, tanggal_selesai)');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti_tambahan.id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_permohonan_cuti_tambahan');
    }
}
