<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220304093251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_permohonan_cuti (id UUID NOT NULL, jns_cuti_id INT NOT NULL, nomor_ticket VARCHAR(50) NOT NULL, nama VARCHAR(50) NOT NULL, pegawai_id UUID NOT NULL, nip18 VARCHAR(18) DEFAULT NULL, nip9 VARCHAR(9) DEFAULT NULL, tanggal_mulai DATE DEFAULT NULL, tanggal_selesai DATE DEFAULT NULL, lama_cuti DOUBLE PRECISION DEFAULT NULL, kantor VARCHAR(128) DEFAULT NULL, unit_organisasi VARCHAR(128) DEFAULT NULL, alasan TEXT DEFAULT NULL, masa_kerja VARCHAR(50) DEFAULT NULL, alamat_sementara TEXT DEFAULT NULL, persalinan_ke INT DEFAULT NULL, tahun VARCHAR(4) NOT NULL, tempat VARCHAR(255) DEFAULT NULL, jabatan VARCHAR(128) DEFAULT NULL, atasan_langsung UUID DEFAULT NULL, nip_atasan_langsung VARCHAR(18) DEFAULT NULL, nama_atasan_langsung VARCHAR(128) DEFAULT NULL, approval_atasan_langsung INT DEFAULT NULL, atasan_berwenang UUID DEFAULT NULL, nip_atasan_berwenang VARCHAR(18) DEFAULT NULL, nama_atasan_berwenang VARCHAR(128) DEFAULT NULL, approval_atasan_berwenang INT DEFAULT NULL, tanggal_buat DATE NOT NULL, nama_buat UUID NOT NULL, status INT NOT NULL, yth VARCHAR(255) DEFAULT NULL, tempat_pejabat VARCHAR(255) DEFAULT NULL, sisa_tahun_lalu1 INT DEFAULT NULL, sisa_tahun_lalu2 INT DEFAULT NULL, sisa_tahun_ini INT DEFAULT NULL, nm_pangkat VARCHAR(255) DEFAULT NULL, tanggal_persetujuan DATE DEFAULT NULL, nama_setuju UUID DEFAULT NULL, alasan_persetujuan TEXT DEFAULT NULL, tgl_srt_izin_cuti DATE DEFAULT NULL, no_srt_izin_cuti VARCHAR(255) DEFAULT NULL, alasan_pembatalan TEXT DEFAULT NULL, cuti_diambil DOUBLE PRECISION DEFAULT NULL, kantor_id UUID DEFAULT NULL, no_sket_dokter VARCHAR(100) DEFAULT NULL, status_perawatan VARCHAR(1) DEFAULT NULL, rumah_sakit VARCHAR(255) DEFAULT NULL, tanggal_perawatan_mulai DATE DEFAULT NULL, tanggal_perawatan_selesai DATE DEFAULT NULL, keguguran VARCHAR(1) DEFAULT NULL, ket_dokter VARCHAR(1) DEFAULT NULL, status_alasan_penting VARCHAR(1) DEFAULT NULL, sisa_total_max DOUBLE PRECISION DEFAULT NULL, lama_cuti_batal DOUBLE PRECISION DEFAULT NULL, status_cuti VARCHAR(1) DEFAULT NULL, no_surat_batal VARCHAR(100) DEFAULT NULL, tgl_surat_batal DATE DEFAULT NULL, nama_pejabat_batal VARCHAR(100) DEFAULT NULL, nip_pejabat_batal VARCHAR(18) DEFAULT NULL, tanggal_mulai_tugas DATE DEFAULT NULL, tanggal_selesai_tugas DATE DEFAULT NULL, sampai VARCHAR(1) DEFAULT NULL, jns_st VARCHAR(1) DEFAULT NULL, jumlah_cuti_ditangguhkan_lt INT DEFAULT NULL, jumlah_cuti_lt INT DEFAULT NULL, tahun_lt VARCHAR(4) DEFAULT NULL, tgl_berangkat_haji DATE DEFAULT NULL, tgl_kembali_haji DATE DEFAULT NULL, tipe_cuti_half INT DEFAULT NULL, kebenaran_upk INT DEFAULT NULL, kd_dati2 UUID DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1CD8142161377D47 ON t_permohonan_cuti (jns_cuti_id)');
        $this->addSql('CREATE INDEX idx_permohonan_cuti ON t_permohonan_cuti (id, nomor_ticket, tanggal_mulai, tanggal_selesai, pegawai_id)');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.pegawai_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.atasan_langsung IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.atasan_berwenang IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.nama_buat IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.nama_setuju IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.kantor_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_permohonan_cuti.kd_dati2 IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD CONSTRAINT FK_1CD8142161377D47 FOREIGN KEY (jns_cuti_id) REFERENCES t_jns_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_permohonan_cuti');
    }
}
