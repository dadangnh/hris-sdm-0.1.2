<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112083131 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE t_jns_cuti_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE t_jam_kerja (id UUID NOT NULL, nomor_ticket VARCHAR(50) NOT NULL, tanggal_mulai DATE NOT NULL, tanggal_selesai DATE NOT NULL, jam_masuk TIME(0) WITHOUT TIME ZONE NOT NULL, jam_pulang TIME(0) WITHOUT TIME ZONE NOT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_by UUID DEFAULT NULL, date_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, approved_by UUID DEFAULT NULL, status INT DEFAULT NULL, keterangan TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_jam_kerja ON t_jam_kerja (id, nomor_ticket, tanggal_mulai, tanggal_selesai)');
        $this->addSql('COMMENT ON COLUMN t_jam_kerja.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_jam_kerja.created_by IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_jam_kerja.approved_by IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE t_jam_kerja_cuti (id UUID NOT NULL, jenis_cuti_id INT DEFAULT NULL, nomor_ticket VARCHAR(50) NOT NULL, tanggal_mulai DATE NOT NULL, tanggal_selesai DATE NOT NULL, jam_masuk_cuti TIME(0) WITHOUT TIME ZONE NOT NULL, jam_pulang_cuti TIME(0) WITHOUT TIME ZONE NOT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_by UUID DEFAULT NULL, date_approved TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, approved_by UUID DEFAULT NULL, status INT DEFAULT NULL, keterangan TEXT DEFAULT NULL, type_cuti INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6EF62C9ED39C45B2 ON t_jam_kerja_cuti (jenis_cuti_id)');
        $this->addSql('CREATE INDEX idx_jam_kerja_cuti ON t_jam_kerja_cuti (id, nomor_ticket, tanggal_mulai, tanggal_selesai)');
        $this->addSql('COMMENT ON COLUMN t_jam_kerja_cuti.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_jam_kerja_cuti.created_by IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_jam_kerja_cuti.approved_by IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE t_jns_cuti (id INT NOT NULL, nama_cuti VARCHAR(75) NOT NULL, jns_cuti INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_jns_cuti ON t_jns_cuti (id, nama_cuti, jns_cuti)');
        $this->addSql('ALTER TABLE t_jam_kerja_cuti ADD CONSTRAINT FK_6EF62C9ED39C45B2 FOREIGN KEY (jenis_cuti_id) REFERENCES t_jns_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_jam_kerja_cuti DROP CONSTRAINT FK_6EF62C9ED39C45B2');
        $this->addSql('DROP SEQUENCE t_jns_cuti_id_seq CASCADE');
        $this->addSql('DROP TABLE t_jam_kerja');
        $this->addSql('DROP TABLE t_jam_kerja_cuti');
        $this->addSql('DROP TABLE t_jns_cuti');
    }
}
