<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211025095813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE t_usulan_hari_libur ADD unit_org UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ADD kantor UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE t_usulan_hari_libur ADD kantor_induk UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.unit_org IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.kantor IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_usulan_hari_libur.kantor_induk IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_usulan_hari_libur DROP unit_org');
        $this->addSql('ALTER TABLE t_usulan_hari_libur DROP kantor');
        $this->addSql('ALTER TABLE t_usulan_hari_libur DROP kantor_induk');
    }
}
