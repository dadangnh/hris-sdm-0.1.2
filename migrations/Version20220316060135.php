<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220316060135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE t_cuti_pegawai ADD jns_cuti_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE t_cuti_pegawai ADD CONSTRAINT FK_5AB09CD561377D47 FOREIGN KEY (jns_cuti_id) REFERENCES t_jns_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5AB09CD561377D47 ON t_cuti_pegawai (jns_cuti_id)');
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD jns_cuti_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE t_permohonan_cuti ADD CONSTRAINT FK_1CD8142161377D47 FOREIGN KEY (jns_cuti_id) REFERENCES t_jns_cuti (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1CD8142161377D47 ON t_permohonan_cuti (jns_cuti_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE t_permohonan_cuti DROP CONSTRAINT FK_1CD8142161377D47');
        $this->addSql('DROP INDEX IDX_1CD8142161377D47');
        $this->addSql('ALTER TABLE t_permohonan_cuti DROP jns_cuti_id');
        $this->addSql('ALTER TABLE t_cuti_pegawai DROP CONSTRAINT FK_5AB09CD561377D47');
        $this->addSql('DROP INDEX IDX_5AB09CD561377D47');
        $this->addSql('ALTER TABLE t_cuti_pegawai DROP jns_cuti_id');
    }
}
