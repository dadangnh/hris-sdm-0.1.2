<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220127042201 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE t_rekap_cuti (id UUID NOT NULL, pegawai_id UUID NOT NULL, tahun INT NOT NULL, sisa_cuti DOUBLE PRECISION DEFAULT NULL, date_created DATE DEFAULT NULL, created_by UUID DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_rekap_cuti ON t_rekap_cuti (id, tahun, pegawai_id)');
        $this->addSql('COMMENT ON COLUMN t_rekap_cuti.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_rekap_cuti.pegawai_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN t_rekap_cuti.created_by IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE t_rekap_cuti');
    }
}
