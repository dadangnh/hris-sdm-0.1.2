<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220518035446 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX idx_permohonan_cuti');
        $this->addSql('CREATE INDEX idx_permohonan_cuti ON t_permohonan_cuti (id, nomor_ticket, tanggal_mulai, tanggal_selesai, pegawai_id, kode_nomor, nomor_surat, tahun_surat)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX idx_permohonan_cuti');
        $this->addSql('CREATE INDEX idx_permohonan_cuti ON t_permohonan_cuti (id, nomor_ticket, tanggal_mulai, tanggal_selesai, pegawai_id)');
    }
}
