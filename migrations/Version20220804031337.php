<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220804031337 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_5ab09cd5d5cc105');
        $this->addSql('CREATE INDEX IDX_5AB09CD5D5CC105 ON t_cuti_pegawai (permohonan_cuti_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_5AB09CD5D5CC105');
        $this->addSql('CREATE UNIQUE INDEX uniq_5ab09cd5d5cc105 ON t_cuti_pegawai (permohonan_cuti_id)');
    }
}
