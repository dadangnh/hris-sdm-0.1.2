<?php

namespace App\EventListener;

use App\Entity\CutiPegawai;
use App\Entity\PermohonanCuti;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

class PermohonanCutiEntityListener
{
    private $logger;
    private $doctrine;

    public function __construct(LoggerInterface $logger, ManagerRegistry $doctrine)
    {
        $this->logger   = $logger;
        $this->doctrine = $doctrine;
    }

    public function preUpdate(PermohonanCuti $permohonanCuti, LifecycleEventArgs $event): void
    {
        $permohonanCuti->generatedNoSI($event);
        $cutiPegawai = $permohonanCuti->getCutiPegawai();
        //$permohonanCuti->createPenundaanForCLT($this->doctrine);
        /*$this->logger->error('preup'.$permohonanCuti->getStatus().'|'.count($cutiPegawai));

        if(2 === $permohonanCuti->getStatus() && 9 === $permohonanCuti->getJnsCuti()->getJnsCuti() && 0 === count($cutiPegawai)){
            $this->logger->error('preupin'.$permohonanCuti->getStatus().'|'.$permohonanCuti->getJnsCuti()->getJnsCuti());
            $riwayatCuti = new CutiPegawai();
            $riwayatCuti->setNomorTicket($permohonanCuti->getNomorTicket());
            $riwayatCuti->setJnsCuti($permohonanCuti->getJnsCuti());
            $riwayatCuti->setPermohonanCuti($permohonanCuti);
            $riwayatCuti->setPegawaiId($permohonanCuti->getPegawaiId());
            $riwayatCuti->setNip9($permohonanCuti->getNip9());
            $riwayatCuti->setTglSrtIzinCuti($permohonanCuti->getTglSrtIzinCuti());
            $riwayatCuti->setNoSketDokter($permohonanCuti->getNoSrtIzinCuti());
            $riwayatCuti->setTglAwalCutiPeg($permohonanCuti->getTanggalMulai());
            $riwayatCuti->setTglAkhirCutiPeg($permohonanCuti->getTanggalSelesai());
            $riwayatCuti->setSisaCuti($permohonanCuti->getJumlahCutiDitangguhkanLt());
            $riwayatCuti->setTahun($permohonanCuti->getTahun());
            $riwayatCuti->setNamaBuat($permohonanCuti->getNamaBuat());
            $riwayatCuti->setTanggalBuat($permohonanCuti->getTanggalBuat());

            $this->doctrine->getManager()->persist($riwayatCuti);
            $this->doctrine->getManager()->flush();
        }*/
    }
}
