<?php
namespace App\Helper;

use Exception;

class AppHelper
{
    /**
     * @param int $length
     * @return string
     * @throws Exception
     */
    public static function RandomString(int $length): string
    {
        $keys = array_merge(range('A', 'Z'));

        $key = '';
        for($i = 0; $i < $length; $i++) {
            $key .= $keys[random_int(0, count($keys) - 1)];
        }
        return $key;
    }
}
