<?php

declare(strict_types=1);

namespace App\OpenApi;


use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\OpenApi;
use ArrayObject;

final class AuthenticationDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['WhoAmIResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'data' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);

        $whoAmIItem = new PathItem(
            ref: 'Who Am I?',
            post: new Operation(
                operationId: 'postCredentialsItem',
                tags: ['Authentication - Get User Data From Token'],
                responses: [
                    '200' => [
                        'description' => 'Status',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/WhoAmIResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Get User Object From Token.',
                requestBody: null,
            ),
        );

        $openApi->getPaths()->addPath('/whoami', $whoAmIItem);

        return $openApi;
    }
}
