<?php

namespace App\Repository;

use App\Entity\CutiPegawai;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CutiPegawai|null find($id, $lockMode = null, $lockVersion = null)
 * @method CutiPegawai|null findOneBy(array $criteria, array $orderBy = null)
 * @method CutiPegawai[]    findAll()
 * @method CutiPegawai[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CutiPegawaiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CutiPegawai::class);
    }

    // /**
    //  * @return CutiPegawai[] Returns an array of CutiPegawai objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param $pegawaiId
     * @param $tahunIni
     * @param $tanggalAcuan
     * @return mixed
     * @throws Exception
     */
    public function findSummaryCutiByPegawaiIdAndTahun($pegawaiId, $tahunIni, $tanggalAcuan): mixed
    {
        $conn = $this->getEntityManager()->getConnection();
        if (empty($tanggalAcuan)) {
            //when the process is calculated based on leave history
            $sql = '
                SELECT sum(case
                           when jns_cuti = :cuti_tahunan then COALESCE(cuti_diambil, 0)
                           else 0 end) as cuti_tahunan,
                   sum(case
                           when jns_cuti = :cuti_setengah then COALESCE(cuti_diambil, 0)
                           else 0 end) as cuti_tahunan_setengah,
                   sum(case
                           when jns_cuti = :cap then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_alasan_penting,
                   sum(case
                           when jns_cuti = :cuti_melahirkan then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_bersalin,
                   sum(case
                           when jns_cuti in (:cuti_sakit_l14, :cuti_sakit_k14) then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_sakit,
                   sum(case
                           when jns_cuti = :cuti_besar then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_besar,
                   sum(case
                           when jns_cuti = :cuti_tahunan
                                and (status = :batal_permohonan or status=:batal)
                                and status_cuti = :panggilan_dinas
                               then COALESCE(CAST(lama_cuti_batal as int), 0)
                           else 0 end) as cuti_tahunan_batal,
                   sum(case
                           when jns_cuti = :cuti_tahunan_lintas_tahun
                               and to_char(tanggal_mulai, :format_tahun) = :tahun_ini
                               then COALESCE(lama_cuti, 0) - COALESCE(jumlah_cuti_lt, 0)
                           else 0 end) as cuti_tahunan_ak,
                   sum(case
                           when jns_cuti = :cuti_tahunan_lintas_tahun and tahun_lt = :tahun_ini
                               then COALESCE(jumlah_cuti_lt, 0)
                           else 0 end) as cuti_tahunan_aw
            FROM t_permohonan_cuti as a
            LEFT JOIN t_jns_cuti as b ON b.id = a.jns_cuti_id
            WHERE pegawai_id = :pegawaiId
              AND (to_char(tanggal_mulai, :format_tahun) = :tahun_ini OR tahun_lt = :tahun_ini)
              AND COALESCE(status, 0) not in (3,4);
            ';
            // when the process is calculated based on the leave application
            $stmt       = $conn->prepare($sql);
            $resultSet  = $stmt->executeQuery([
                'cuti_tahunan'              => 6,
                'cuti_setengah'             => 10,
                'cap'                       => 4,
                'cuti_melahirkan'           => 3,
                'cuti_sakit_l14'            => 2,
                'cuti_sakit_k14'            => 7,
                'cuti_besar'                => 1,
                'panggilan_dinas'           => 2,
                'cuti_tahunan_lintas_tahun' => 9,
                'tahun_ini'                 => $tahunIni,
                'pegawaiId'                 => $pegawaiId,
                'batal_permohonan'          => 3,
                'batal'                     => 4,
                'format_tahun'              => 'yyyy'
            ]);
        } else {
            $sql='
                SELECT sum(case
                           when jns_cuti = :cuti_tahunan then COALESCE(cuti_diambil, 0)
                           else 0 end) as cuti_tahunan,
                   sum(case
                           when jns_cuti = :cuti_setengah then COALESCE(cuti_diambil, 0)
                           else 0 end) as cuti_tahunan_setengah,
                   sum(case
                           when jns_cuti = :cap then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_alasan_penting,
                   sum(case
                           when jns_cuti = :cuti_melahirkan then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_bersalin,
                   sum(case
                           when jns_cuti in (:cuti_sakit_l14, :cuti_sakit_k14) then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_sakit,
                   sum(case
                           when jns_cuti = :cuti_besar then COALESCE(lama_cuti, 0)
                           else 0 end) as cuti_besar,
                   sum(case
                           when jns_cuti = :cuti_tahunan
                                and (status = :batal_permohonan or status=:batal)
                                and status_cuti = :panggilan_dinas
                               then COALESCE(CAST(lama_cuti_batal as int), 0)
                           else 0 end) as cuti_tahunan_batal,
                   sum(case
                           when jns_cuti = :cuti_tahunan_lintas_tahun
                               and to_char(tanggal_mulai, :format_tahun) = :tahun_ini
                               then COALESCE(lama_cuti, 0) - COALESCE(jumlah_cuti_lt, 0)
                           else 0 end) as cuti_tahunan_ak,
                   sum(case
                           when jns_cuti = :cuti_tahunan_lintas_tahun and tahun_lt = :tahun_ini
                               then COALESCE(jumlah_cuti_lt, 0)
                           else 0 end) as cuti_tahunan_aw
            FROM t_permohonan_cuti as a
            LEFT JOIN t_jns_cuti as b ON b.id = a.jns_cuti_id
            WHERE pegawai_id = :pegawaiId
              AND (to_char(tanggal_mulai, :format_tahun) = :tahun_ini OR tahun_lt = :tahun_ini)
              AND (to_char(tanggal_mulai, :format_tanggal) < :tanggal_acuan)
              AND COALESCE(status, 0) not in (3,4);
            ';
            // when the process is calculated based on the leave application
            $stmt       = $conn->prepare($sql);
            $resultSet  = $stmt->executeQuery([
                'cuti_tahunan'              => 6,
                'cuti_setengah'             => 10,
                'cap'                       => 4,
                'cuti_melahirkan'           => 3,
                'cuti_sakit_l14'            => 2,
                'cuti_sakit_k14'            => 7,
                'cuti_besar'                => 1,
                'panggilan_dinas'           => 2,
                'cuti_tahunan_lintas_tahun' => 9,
                'tahun_ini'                 => $tahunIni,
                'pegawaiId'                 => $pegawaiId,
                'batal_permohonan'          => 3,
                'batal'                     => 4,
                'format_tahun'              => 'yyyy',
                'format_tanggal'            => 'yyyymmdd',
                'tanggal_acuan'             => $tanggalAcuan
            ]);
        }

        return $resultSet->fetchAssociative();
    }

    /**
     * @param $pegawaiId
     * @param $tahun_ini
     * @param $tanggalAcuan
     * @return mixed
     * @throws Exception
     */
    public function findCutiLintasTahunPanggilanDinas($pegawaiId, $tahun_ini, $tanggalAcuan): mixed
    {
        $tahun_lalu = $tahun_ini - 1;

        $entityManager = $this->getEntityManager();
        $conn = $this->getEntityManager()->getConnection();
        if (empty($tanggalAcuan)) {
            $sql = '
                SELECT *
                FROM t_permohonan_cuti
                WHERE pegawai_id = :pegawaiId
                  AND jns_cuti_id = :jns_cuti_id
                  AND status_cuti = :status_cuti
                  AND (
                        (to_char(tanggal_mulai, :format_tahun) = :tahun_ini
                            AND to_char(tanggal_mulai_tugas, :format_tahun) = :tahun_ini
                            AND tahun = :tahun_ini
                            )
                        OR (tahun_lt = :tahun_ini AND to_char(tanggal_mulai_tugas, :format_tahun) = :tahun_lalu)
                        OR (tahun_lt = :tahun_ini AND to_char(tanggal_mulai_tugas, :format_tahun) = :tahun_ini)
                    )
            ';
            $stmt       = $conn->prepare($sql);
            $resultSet  = $stmt->executeQuery([
                'pegawaiId'     => $pegawaiId,
                'jns_cuti_id'   => 9,
                'format_tahun'  => 'yyyy',
                'tahun_ini'     => $tahun_ini,
                'tahun_lalu'    => $tahun_lalu,
                'status_cuti'   => 2
            ]);
        } else {
            $sql = '
                SELECT *
                FROM t_permohonan_cuti
                WHERE pegawai_id = :pegawaiId
                  AND jns_cuti_id = :jns_cuti_id
                  AND status_cuti = :status_cuti
                  AND (
                        (to_char(tanggal_mulai, :format_tahun) = :tahun_ini
                            AND to_char(tanggal_mulai_tugas, :format_tahun) = :tahun_ini
                            AND tahun = :tahun_ini
                            )
                        OR (tahun_lt = :tahun_ini AND to_char(tanggal_mulai_tugas, :format_tahun) = :tahun_lalu)
                        OR (tahun_lt = :tahun_ini AND to_char(tanggal_mulai_tugas, :format_tahun) = :tahun_ini)
                    )
                  AND (to_char(tanggal_mulai, :format_tanggal) < :tanggal_acuan)
            ';
            $stmt       = $conn->prepare($sql);
            $resultSet  = $stmt->executeQuery([
                'pegawaiId'         => $pegawaiId,
                'jns_cuti_id'       => 9,
                'format_tahun'      => 'yyyy',
                'tahun_ini'         => $tahun_ini,
                'tahun_lalu'        => $tahun_lalu,
                'status_cuti'       => 2,
                'format_tanggal'    => 'yyyymmdd',
                'tanggal_acuan'     => $tanggalAcuan
            ]);
        }
        return $resultSet->fetchAllAssociative();
    }

    /**
     * @param $provinsi
     * @param $kota
     * @param $agama
     * @return mixed
     * @throws Exception
     */
    public function findHariLibur($provinsi, $kota, $agama): mixed
    {
        $entityManager  = $this->getEntityManager();
        $conn           = $this->getEntityManager()->getConnection();
        $sql            = "
            select tanggal_awal, tanggal_akhir
            from (
                  select tanggal_awal, tanggal_akhir, kota_ids as kota,
                       provinsi_ids as propinsi, agama_ids as nm_agama
                  from t_hari_libur
                  where scope = 0
                     or (provinsi_ids is null and agama_ids is null and kota_ids in (:kota))
                     or (provinsi_ids is null and agama_ids in (:agama) and
                         kota_ids is null)
                     or (provinsi_ids in (:provinsi) and agama_ids is null and
                         kota_ids is null)
                     or ((provinsi_ids in (:provinsi)
                      and agama_ids in (:agama) and kota_ids is null)
                      )
                  union all
                  select tanggal_mulai as tanggal_awal, tanggal_selesai as tanggal_akhir, '' as kota,
                                '' as propinsi, '' as nm_agama
                from t_cuti_bersama
            ) as x
            group by tanggal_awal, tanggal_akhir
            order by tanggal_awal
        ";
        $stmt       = $conn->prepare($sql);

        $resultSet  = $stmt->executeQuery([
            'kota'      => $kota,
            'agama'     => $agama,
            'provinsi'  => $provinsi
        ]);

        return $resultSet->fetchAllAssociative();
    }

    /**
     * @param $permohonanId
     * @return bool
     * @throws Exception
     */
    public function deleteByPermohonanId($permohonanId)
    {
        $entityManager  = $this->getEntityManager();
        $conn           = $this->getEntityManager()->getConnection();
        $sql            = "
            delete from t_cuti_pegawai where permohonan_cuti_id = :permohonanId
        ";

        $stmt       = $conn->prepare($sql);

        $resultSet  = $stmt->executeQuery([
            'permohonanId'      => $permohonanId
        ]);

        return true;
    }
}
