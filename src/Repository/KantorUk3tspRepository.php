<?php

namespace App\Repository;

use App\Entity\KantorUk3tsp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method KantorUk3tsp|null find($id, $lockMode = null, $lockVersion = null)
 * @method KantorUk3tsp|null findOneBy(array $criteria, array $orderBy = null)
 * @method KantorUk3tsp[]    findAll()
 * @method KantorUk3tsp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KantorUk3tspRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KantorUk3tsp::class);
    }

    // /**
    //  * @return KantorUk3tsp[] Returns an array of KantorUk3tsp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KantorUk3tsp
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
