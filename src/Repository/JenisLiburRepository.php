<?php

namespace App\Repository;

use App\Entity\JenisLibur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JenisLibur|null find($id, $lockMode = null, $lockVersion = null)
 * @method JenisLibur|null findOneBy(array $criteria, array $orderBy = null)
 * @method JenisLibur[]    findAll()
 * @method JenisLibur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JenisLiburRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JenisLibur::class);
    }

    // /**
    //  * @return JenisLibur[] Returns an array of JenisLibur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JenisLibur
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
