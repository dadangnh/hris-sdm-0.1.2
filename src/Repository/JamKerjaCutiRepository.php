<?php

namespace App\Repository;

use App\Entity\JamKerjaCuti;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JamKerjaCuti|null find($id, $lockMode = null, $lockVersion = null)
 * @method JamKerjaCuti|null findOneBy(array $criteria, array $orderBy = null)
 * @method JamKerjaCuti[]    findAll()
 * @method JamKerjaCuti[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JamKerjaCutiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JamKerjaCuti::class);
    }

    // /**
    //  * @return JamKerjaCuti[] Returns an array of JamKerjaCuti objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JamKerjaCuti
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
