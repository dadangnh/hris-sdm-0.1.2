<?php

namespace App\Repository;

use App\Entity\JnsCuti;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JnsCuti|null find($id, $lockMode = null, $lockVersion = null)
 * @method JnsCuti|null findOneBy(array $criteria, array $orderBy = null)
 * @method JnsCuti[]    findAll()
 * @method JnsCuti[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JnsCutiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JnsCuti::class);
    }

    // /**
    //  * @return JnsCuti[] Returns an array of JnsCuti objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JnsCuti
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
