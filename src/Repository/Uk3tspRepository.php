<?php

namespace App\Repository;

use App\Entity\Uk3tsp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Uk3tsp|null find($id, $lockMode = null, $lockVersion = null)
 * @method Uk3tsp|null findOneBy(array $criteria, array $orderBy = null)
 * @method Uk3tsp[]    findAll()
 * @method Uk3tsp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Uk3tspRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Uk3tsp::class);
    }

    // /**
    //  * @return Uk3tsp[] Returns an array of Uk3tsp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Uk3tsp
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @param $keyword
     * @return mixed
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findPermohonanById($keyword): mixed
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.id =:val')
            ->setParameter('val', $keyword)
            ->addOrderBy('j.pmk', 'ASC')
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findActivePmk(): mixed
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.status = 2')
            ->addOrderBy('j.tanggal', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
