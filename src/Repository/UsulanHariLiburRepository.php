<?php

namespace App\Repository;

use App\Entity\UsulanHariLibur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsulanHariLibur|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsulanHariLibur|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsulanHariLibur[]    findAll()
 * @method UsulanHariLibur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsulanHariLiburRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsulanHariLibur::class);
    }

    // /**
    //  * @return UsulanHariLibur[] Returns an array of UsulanHariLibur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsulanHariLibur
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
