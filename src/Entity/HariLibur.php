<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\HariLiburRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: HariLiburRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_hari_libur'
)]
#[ORM\Index(
    columns: [
        'id',
        'tanggal_awal',
        'tanggal_akhir',
        'jenis_id',
        'scope'
    ],
    name: 'idx_hari_libur'
)]
#[ORM\Index(
    columns: [
        'id',
        'provinsi_ids',
        'kota_ids',
        'kantor_ids',
        'agama_ids'
    ],
    name: 'idx_hari_libur_ref'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'usulan.id' => 'exact',
        'keterangan' => 'ipartial',
        'jenis.id' => 'exact',
        'provinsiIds' => 'exact',
        'kotaIds' => 'exact',
        'kantorIds' => 'exact',
        'agamaIds' => 'exact',
        'usulan.nomorSuratDasar' => 'ipartial',
        'usulan.unitOrg' => 'exact',
        'usulan.kantor' => 'exact',
        'usulan.kantorInduk' => 'exact'
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggalAwal',
        'tanggalAkhir'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'scope',
        'usulan.status',
        'usulan.nomorTicket'
    ]
)]
#[ApiFilter(
    filterClass: BooleanFilter::class,
    properties: ['active']
)]
class HariLibur
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private UuidV6 $id;

    #[ORM\OneToOne(
        inversedBy: 'hariLibur',
        targetEntity: UsulanHariLibur::class,
        cascade: [
            'persist',
            'remove'
        ]
    )]
    #[ORM\JoinColumn(
        nullable: false
    )]
    private ?UsulanHariLibur $usulan;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?DateTimeInterface $tanggalAwal;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?DateTimeInterface $tanggalAkhir;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?string $keterangan;

    #[ORM\ManyToOne(
        targetEntity: JenisLibur::class,
        inversedBy: 'hariLiburs'
    )]
    #[ORM\JoinColumn(
        nullable: false
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?JenisLibur $jenis;

    #[ORM\Column(
        type: 'integer'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?int $scope;

    #[ORM\Column(
        type: 'simple_array',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private array $provinsiIds = [];

    #[ORM\Column(
        type: 'simple_array',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private array $kotaIds = [];

    #[ORM\Column(
        type: 'simple_array',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private array $kantorIds = [];

    #[ORM\Column(
        type: 'simple_array',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private array $agamaIds = [];

    #[ORM\Column(
        type: 'boolean'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read'
        ]
    )]
    private ?bool $active;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getUsulan(): ?UsulanHariLibur
    {
        return $this->usulan;
    }

    public function setUsulan(UsulanHariLibur $usulan): self
    {
        $this->usulan = $usulan;

        return $this;
    }

    public function getTanggalAwal(): ?DateTimeInterface
    {
        return $this->tanggalAwal;
    }

    public function setTanggalAwal(DateTimeInterface $tanggalAwal): self
    {
        $this->tanggalAwal = $tanggalAwal;

        return $this;
    }

    public function getTanggalAkhir(): ?DateTimeInterface
    {
        return $this->tanggalAkhir;
    }

    public function setTanggalAkhir(DateTimeInterface $tanggalAkhir): self
    {
        $this->tanggalAkhir = $tanggalAkhir;

        return $this;
    }

    public function getKeterangan(): ?string
    {
        return $this->keterangan;
    }

    public function setKeterangan(?string $keterangan): self
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    public function getJenis(): ?JenisLibur
    {
        return $this->jenis;
    }

    public function setJenis(?JenisLibur $jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }

    public function getScope(): ?int
    {
        return $this->scope;
    }

    public function setScope(int $scope): self
    {
        $this->scope = $scope;

        return $this;
    }

    public function getProvinsiIds(): ?array
    {
        return $this->provinsiIds;
    }

    public function setProvinsiIds(?array $provinsiIds): self
    {
        $this->provinsiIds = $provinsiIds;

        return $this;
    }

    public function getKotaIds(): ?array
    {
        return $this->kotaIds;
    }

    public function setKotaIds(?array $kotaIds): self
    {
        $this->kotaIds = $kotaIds;

        return $this;
    }

    public function getKantorIds(): ?array
    {
        return $this->kantorIds;
    }

    public function setKantorIds(?array $kantorIds): self
    {
        $this->kantorIds = $kantorIds;

        return $this;
    }

    public function getAgamaIds(): ?array
    {
        return $this->agamaIds;
    }

    public function setAgamaIds(?array $agamaIds): self
    {
        $this->agamaIds = $agamaIds;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
