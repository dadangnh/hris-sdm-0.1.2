<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Helper\AppHelper;
use App\Repository\JamKerjaCutiRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')',
    denormalizationContext: [
        'groups' => ['jamkerjacuti:write'], '
        swagger_definition_name' => 'write'
    ],
    normalizationContext: [
        'groups' => ['jamkerjacuti:read'],
        'swagger_definition_name' => 'read'
    ]
)]
#[ORM\Entity(
    repositoryClass: JamKerjaCutiRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_jam_kerja_cuti'
)]
#[ORM\Index(
    columns: [
        'id',
        'nomor_ticket',
        'tanggal_mulai',
        'tanggal_selesai'
    ],
    name: 'idx_jam_kerja_cuti'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nomorTicket' => 'ipartial',
        'keterangan' => 'ipartial'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: ['status']
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggalMulai',
        'tanggalSelesai'
    ]
)]
class JamKerjaCuti
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'string', length: 50
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?string $nomorTicket;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalMulai;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalSelesai;

    #[ORM\ManyToOne(
        targetEntity: JnsCuti::class,
        inversedBy: 'jamKerjaCuti'
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?jnsCuti $jenisCuti;

    #[ORM\Column(
        type: 'time'
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?DateTimeInterface $jamMasukCuti;

    #[ORM\Column(
        type: 'time'
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?DateTimeInterface $jamPulangCuti;

    #[ORM\Column(
        type: 'datetime',
        nullable: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?DateTimeInterface $dateCreated;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private $createdBy;

    #[ORM\Column(
        type: 'datetime',
        nullable: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?DateTimeInterface $dateApproved;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private $approvedBy;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?int $status;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?string $keterangan;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'jamkerjacuti:write'
        ]
    )]
    private ?int $typeCuti;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getNomorTicket(): ?string
    {
        return $this->nomorTicket;
    }

    public function setNomorTicket(string $nomorTicket): self
    {
        $this->nomorTicket = $nomorTicket;

        return $this;
    }

    /**
     * @throws Exception
     */
    #[ORM\PrePersist]
    public function setNomorTicketValue(): void
    {
        $this->nomorTicket = 'JKC-' . AppHelper::RandomString(3) . round(microtime(true));
    }

    public function getTanggalMulai(): ?DateTimeInterface
    {
        return $this->tanggalMulai;
    }

    public function setTanggalMulai(DateTimeInterface $tanggalMulai): self
    {
        $this->tanggalMulai = $tanggalMulai;

        return $this;
    }

    public function getTanggalSelesai(): ?DateTimeInterface
    {
        return $this->tanggalSelesai;
    }

    public function setTanggalSelesai(DateTimeInterface $tanggalSelesai): self
    {
        $this->tanggalSelesai = $tanggalSelesai;

        return $this;
    }

    public function getJenisCuti(): ?jnsCuti
    {
        return $this->jenisCuti;
    }

    public function setJenisCuti(?jnsCuti $jenisCuti): self
    {
        $this->jenisCuti = $jenisCuti;

        return $this;
    }

    public function getJamMasukCuti(): ?DateTimeInterface
    {
        return $this->jamMasukCuti;
    }

    public function setJamMasukCuti(DateTimeInterface $jamMasukCuti): self
    {
        $this->jamMasukCuti = $jamMasukCuti;

        return $this;
    }

    public function getJamPulangCuti(): ?DateTimeInterface
    {
        return $this->jamPulangCuti;
    }

    public function setJamPulangCuti(DateTimeInterface $jamPulangCuti): self
    {
        $this->jamPulangCuti = $jamPulangCuti;

        return $this;
    }

    public function getDateCreated(): ?DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getDateApproved(): ?DateTimeInterface
    {
        return $this->dateApproved;
    }

    public function setDateApproved(?DateTimeInterface $dateApproved): self
    {
        $this->dateApproved = $dateApproved;

        return $this;
    }

    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    public function setApprovedBy($approvedBy): self
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getKeterangan(): ?string
    {
        return $this->keterangan;
    }

    public function setKeterangan(?string $keterangan): self
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    public function getTypeCuti(): ?int
    {
        return $this->typeCuti;
    }

    public function setTypeCuti(?int $typeCuti): self
    {
        $this->typeCuti = $typeCuti;

        return $this;
    }
}
