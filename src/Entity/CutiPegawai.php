<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\CutiPegawaiRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    normalizationContext: [
        'groups' => ['riwayatCuti:read'],
        'swagger_definition_name' => 'read'
    ],
    denormalizationContext: [
        'groups' => ['riwayatCuti:write'],
        'swagger_definition_name' => 'write'
    ],
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: CutiPegawaiRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_cuti_pegawai'
)]
#[ORM\Index(
    columns: [
        'id',
        'nomor_ticket',
        'tgl_awal_cuti_peg',
        'tgl_akhir_cuti_peg',
        'pegawai_id'
    ],
    name: 'idx_cuti_pegawai'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nomorTicket' => 'ipartial',
        'pegawaiId' => 'exact',
        'nip9' => 'ipartial'
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tglAwalCutiPeg',
        'tglAkhirCutiPeg'
    ]
)]
class CutiPegawai
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'uuid'
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private $pegawaiId;

    #[ORM\Column(
        type: 'string',
        length: 9,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $nip9;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?int $noUrutCutiPeg;

    #[ORM\Column(
        type: 'string',
        length: 50,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $noSrtIzinCuti;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglSrtIzinCuti;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglAwalCutiPeg;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglAkhirCutiPeg;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?float $jumlahCuti;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?float $sisaCuti;

    #[ORM\Column(
        type: 'string',
        length: 4
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $tahun;

    #[ORM\Column(
        type: 'uuid'
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private $namaBuat;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalBuat;

    #[ORM\Column(
        type: 'string',
        length: 50
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $nomorTicket;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?int $batal;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $alasanPembatalan;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?int $persalinanKe;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $noSketDokter;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $statusPerawatan;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $rumahSakit;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalPerawatanMulai;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalPerawatanSelesai;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $keguguran;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $ketDokter;

    #[ORM\Column(
        type: 'string',
        length: 2,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $statusAlasanPenting;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?float $lamaCutiBatal;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalMulaiTugas;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalSelesaiTugas;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?int $jumlahCutiDitangguhkanLt;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?int $jumlahCutiLt;

    #[ORM\Column(
        type: 'string',
        length: 4,
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?string $tahunLt;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglBerangkatHaji;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglKembaliHaji;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?int $tipeCutiHalf;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private $kdDati2;

    #[ORM\ManyToOne(
        targetEntity: PermohonanCuti::class,
        inversedBy: 'cutiPegawai'
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?PermohonanCuti $permohonanCuti;

    #[ORM\ManyToOne(
        targetEntity: JnsCuti::class
    )]
    #[Groups(
        groups: [
            'riwayatCuti:read',
            'riwayatCuti:write'
        ]
    )]
    private ?JnsCuti $jnsCuti;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getPegawaiId()
    {
        return $this->pegawaiId;
    }

    public function setPegawaiId($pegawaiId): self
    {
        $this->pegawaiId = $pegawaiId;

        return $this;
    }

    public function getNip9(): ?string
    {
        return $this->nip9;
    }

    public function setNip9(?string $nip9): self
    {
        $this->nip9 = $nip9;

        return $this;
    }

    public function getNoUrutCutiPeg(): ?int
    {
        return $this->noUrutCutiPeg;
    }

    public function setNoUrutCutiPeg(?int $noUrutCutiPeg): self
    {
        $this->noUrutCutiPeg = $noUrutCutiPeg;

        return $this;
    }

    public function getNoSrtIzinCuti(): ?string
    {
        return $this->noSrtIzinCuti;
    }

    public function setNoSrtIzinCuti(?string $noSrtIzinCuti): self
    {
        $this->noSrtIzinCuti = $noSrtIzinCuti;

        return $this;
    }

    public function getTglSrtIzinCuti(): ?DateTimeInterface
    {
        return $this->tglSrtIzinCuti;
    }

    public function setTglSrtIzinCuti(?DateTimeInterface $tglSrtIzinCuti): self
    {
        $this->tglSrtIzinCuti = $tglSrtIzinCuti;

        return $this;
    }

    public function getTglAwalCutiPeg(): ?DateTimeInterface
    {
        return $this->tglAwalCutiPeg;
    }

    public function setTglAwalCutiPeg(?DateTimeInterface $tglAwalCutiPeg): self
    {
        $this->tglAwalCutiPeg = $tglAwalCutiPeg;

        return $this;
    }

    public function getTglAkhirCutiPeg(): ?DateTimeInterface
    {
        return $this->tglAkhirCutiPeg;
    }

    public function setTglAkhirCutiPeg(?DateTimeInterface $tglAkhirCutiPeg): self
    {
        $this->tglAkhirCutiPeg = $tglAkhirCutiPeg;

        return $this;
    }

    public function getJumlahCuti(): ?float
    {
        return $this->jumlahCuti;
    }

    public function setJumlahCuti(?float $jumlahCuti): self
    {
        $this->jumlahCuti = $jumlahCuti;

        return $this;
    }

    public function getSisaCuti(): ?float
    {
        return $this->sisaCuti;
    }

    public function setSisaCuti(?float $sisaCuti): self
    {
        $this->sisaCuti = $sisaCuti;

        return $this;
    }

    public function getTahun(): ?string
    {
        return $this->tahun;
    }

    public function setTahun(string $tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    public function getNamaBuat()
    {
        return $this->namaBuat;
    }

    public function setNamaBuat($namaBuat): self
    {
        $this->namaBuat = $namaBuat;

        return $this;
    }

    public function getTanggalBuat(): ?DateTimeInterface
    {
        return $this->tanggalBuat;
    }

    public function setTanggalBuat(DateTimeInterface $tanggalBuat): self
    {
        $this->tanggalBuat = $tanggalBuat;

        return $this;
    }

    public function getNomorTicket(): ?string
    {
        return $this->nomorTicket;
    }

    public function setNomorTicket(string $nomorTicket): self
    {
        $this->nomorTicket = $nomorTicket;

        return $this;
    }

    public function getBatal(): ?int
    {
        return $this->batal;
    }

    public function setBatal(?int $batal): self
    {
        $this->batal = $batal;

        return $this;
    }

    public function getAlasanPembatalan(): ?string
    {
        return $this->alasanPembatalan;
    }

    public function setAlasanPembatalan(?string $alasanPembatalan): self
    {
        $this->alasanPembatalan = $alasanPembatalan;

        return $this;
    }

    public function getPersalinanKe(): ?int
    {
        return $this->persalinanKe;
    }

    public function setPersalinanKe(?int $persalinanKe): self
    {
        $this->persalinanKe = $persalinanKe;

        return $this;
    }

    public function getNoSketDokter(): ?string
    {
        return $this->noSketDokter;
    }

    public function setNoSketDokter(?string $noSketDokter): self
    {
        $this->noSketDokter = $noSketDokter;

        return $this;
    }

    public function getStatusPerawatan(): ?string
    {
        return $this->statusPerawatan;
    }

    public function setStatusPerawatan(?string $statusPerawatan): self
    {
        $this->statusPerawatan = $statusPerawatan;

        return $this;
    }

    public function getRumahSakit(): ?string
    {
        return $this->rumahSakit;
    }

    public function setRumahSakit(?string $rumahSakit): self
    {
        $this->rumahSakit = $rumahSakit;

        return $this;
    }

    public function getTanggalPerawatanMulai(): ?DateTimeInterface
    {
        return $this->tanggalPerawatanMulai;
    }

    public function setTanggalPerawatanMulai(?DateTimeInterface $tanggalPerawatanMulai): self
    {
        $this->tanggalPerawatanMulai = $tanggalPerawatanMulai;

        return $this;
    }

    public function getTanggalPerawatanSelesai(): ?DateTimeInterface
    {
        return $this->tanggalPerawatanSelesai;
    }

    public function setTanggalPerawatanSelesai(?DateTimeInterface $tanggalPerawatanSelesai): self
    {
        $this->tanggalPerawatanSelesai = $tanggalPerawatanSelesai;

        return $this;
    }

    public function getKeguguran(): ?string
    {
        return $this->keguguran;
    }

    public function setKeguguran(?string $keguguran): self
    {
        $this->keguguran = $keguguran;

        return $this;
    }

    public function getKetDokter(): ?string
    {
        return $this->ketDokter;
    }

    public function setKetDokter(?string $ketDokter): self
    {
        $this->ketDokter = $ketDokter;

        return $this;
    }

    public function getStatusAlasanPenting(): ?string
    {
        return $this->statusAlasanPenting;
    }

    public function setStatusAlasanPenting(?string $statusAlasanPenting): self
    {
        $this->statusAlasanPenting = $statusAlasanPenting;

        return $this;
    }

    public function getLamaCutiBatal(): ?float
    {
        return $this->lamaCutiBatal;
    }

    public function setLamaCutiBatal(?float $lamaCutiBatal): self
    {
        $this->lamaCutiBatal = $lamaCutiBatal;

        return $this;
    }

    public function getTanggalMulaiTugas(): ?DateTimeInterface
    {
        return $this->tanggalMulaiTugas;
    }

    public function setTanggalMulaiTugas(?DateTimeInterface $tanggalMulaiTugas): self
    {
        $this->tanggalMulaiTugas = $tanggalMulaiTugas;

        return $this;
    }

    public function getTanggalSelesaiTugas(): ?DateTimeInterface
    {
        return $this->tanggalSelesaiTugas;
    }

    public function setTanggalSelesaiTugas(?DateTimeInterface $tanggalSelesaiTugas): self
    {
        $this->tanggalSelesaiTugas = $tanggalSelesaiTugas;

        return $this;
    }

    public function getJumlahCutiDitangguhkanLt(): ?int
    {
        return $this->jumlahCutiDitangguhkanLt;
    }

    public function setJumlahCutiDitangguhkanLt(?int $jumlahCutiDitangguhkanLt): self
    {
        $this->jumlahCutiDitangguhkanLt = $jumlahCutiDitangguhkanLt;

        return $this;
    }

    public function getJumlahCutiLt(): ?int
    {
        return $this->jumlahCutiLt;
    }

    public function setJumlahCutiLt(?int $jumlahCutiLt): self
    {
        $this->jumlahCutiLt = $jumlahCutiLt;

        return $this;
    }

    public function getTahunLt(): ?string
    {
        return $this->tahunLt;
    }

    public function setTahunLt(?string $tahunLt): self
    {
        $this->tahunLt = $tahunLt;

        return $this;
    }

    public function getTglBerangkatHaji(): ?DateTimeInterface
    {
        return $this->tglBerangkatHaji;
    }

    public function setTglBerangkatHaji(?DateTimeInterface $tglBerangkatHaji): self
    {
        $this->tglBerangkatHaji = $tglBerangkatHaji;

        return $this;
    }

    public function getTglKembaliHaji(): ?DateTimeInterface
    {
        return $this->tglKembaliHaji;
    }

    public function setTglKembaliHaji(?DateTimeInterface $tglKembaliHaji): self
    {
        $this->tglKembaliHaji = $tglKembaliHaji;

        return $this;
    }

    public function getTipeCutiHalf(): ?int
    {
        return $this->tipeCutiHalf;
    }

    public function setTipeCutiHalf(?int $tipeCutiHalf): self
    {
        $this->tipeCutiHalf = $tipeCutiHalf;

        return $this;
    }

    public function getKdDati2()
    {
        return $this->kdDati2;
    }

    public function setKdDati2($kdDati2): self
    {
        $this->kdDati2 = $kdDati2;

        return $this;
    }

    public function getPermohonanCuti(): ?PermohonanCuti
    {
        return $this->permohonanCuti;
    }

    public function setPermohonanCuti(?PermohonanCuti $permohonanCuti): self
    {
        $this->permohonanCuti = $permohonanCuti;

        return $this;
    }

    public function getJnsCuti(): ?JnsCuti
    {
        return $this->jnsCuti;
    }

    public function setJnsCuti(?JnsCuti $jnsCuti): self
    {
        $this->jnsCuti = $jnsCuti;

        return $this;
    }
}
