<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\RekapCutiRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: RekapCutiRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_rekap_cuti'
)]
#[ORM\Index(
    columns: [
        'id',
        'tahun',
        'pegawai_id'
    ],
    name: 'idx_rekap_cuti'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: ['pegawaiId' => 'exact']
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: ['tahun']
)]
class RekapCuti
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'uuid'
    )]
    private $pegawaiId;

    #[ORM\Column(
        type: 'integer'
    )]
    private ?int $tahun;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    private ?float $sisaCuti;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    private ?DateTimeInterface $dateCreated;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private $createdBy;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    private ?float $sisaCutiNonMax;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getPegawaiId()
    {
        return $this->pegawaiId;
    }

    public function setPegawaiId($pegawaiId): self
    {
        $this->pegawaiId = $pegawaiId;

        return $this;
    }

    public function getTahun(): ?int
    {
        return $this->tahun;
    }

    public function setTahun(int $tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    public function getSisaCuti(): ?float
    {
        return $this->sisaCuti;
    }

    public function setSisaCuti(?float $sisaCuti): self
    {
        $this->sisaCuti = $sisaCuti;

        return $this;
    }

    public function getDateCreated(): ?DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getSisaCutiNonMax(): ?float
    {
        return $this->sisaCutiNonMax;
    }

    public function setSisaCutiNonMax(?float $sisaCutiNonMax): self
    {
        $this->sisaCutiNonMax = $sisaCutiNonMax;

        return $this;
    }
}
