<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\UsulanHariLiburRepository;
use App\Helper\AppHelper;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    normalizationContext: [
        'groups' => [
            'usulanharilibur:read'
        ],
        'swagger_definition_name' => 'read'
    ],
    denormalizationContext: [
        'groups' => [
            'usulanharilibur:write'
        ],
        'swagger_definition_name' => 'write'
    ],
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: UsulanHariLiburRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_usulan_hari_libur'
)]
#[ORM\Index(
    columns: [
        'id',
        'nomor_surat_dasar',
        'status',
        'nomor_ticket'
    ],
    name: 'idx_usulan_hari_libur'
)]
#[ORM\Index(
    columns: [
        'id',
        'id_pembuat',
        'id_pengupdate',
        'id_atasan_approve',
        'id_kakap_approve',
        'id_upk_pusat_approve'
    ],
    name: 'idx_usulan_hari_libur_ids'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nomorSuratDasar' => 'ipartial',
        'nomorTicket' => 'ipartial',
        'keteranganSuratDasar' => 'ipartial',
        'idPembuat' => 'exact',
        'rolePembuat' => 'exact',
        'idPengupdate' => 'exact',
        'rolePengupdate' => 'exact',
        'idAtasanApprove' => 'exact',
        'idKakapApprove' => 'exact',
        'idUpkPusatApprove' => 'exact',
        'hariLibur.id' => 'exact',
        'unitOrg' => 'exact',
        'kantor' => 'exact',
        'kantorInduk' => 'exact'
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggalSuratDasar',
        'tanggalBuat',
        'tanggalUpdate',
        'tanggalAtasanApprove',
        'tanggalKakapApprove',
        'tanggalUpkPusatApprove'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'status'
    ]
)]
class UsulanHariLibur
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'string',
        length: 120
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?string $nomorSuratDasar;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?DateTimeInterface $tanggalSuratDasar;

    #[ORM\Column(
        type: 'string',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?string $keteranganSuratDasar;

    #[ORM\Column(
        type: 'uuid'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $idPembuat;

    #[ORM\Column(
        type: 'string',
        length: 255
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?string $rolePembuat;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?DateTimeInterface $tanggalBuat;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $idPengupdate;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?string $rolePengupdate;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?DateTimeInterface $tanggalUpdate;

    #[ORM\Column(
        type: 'integer'
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?int $status;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $idAtasanApprove;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?DateTimeInterface $tanggalAtasanApprove;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $idKakapApprove;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?DateTimeInterface $tanggalKakapApprove;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $idUpkPusatApprove;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?DateTimeInterface $tanggalUpkPusatApprove;

    #[ORM\Column(
        type: 'string',
        length: 50,
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?string $nomorTicket;

    #[ORM\OneToOne(
        mappedBy: 'usulan',
        targetEntity: HariLibur::class,
        cascade: [
            'persist',
            'remove'
        ]
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private ?HariLibur $hariLibur;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $unitOrg;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $kantor;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'usulanharilibur:read',
            'usulanharilibur:write'
        ]
    )]
    private $kantorInduk;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getNomorSuratDasar(): ?string
    {
        return $this->nomorSuratDasar;
    }

    public function setNomorSuratDasar(string $nomorSuratDasar): self
    {
        $this->nomorSuratDasar = $nomorSuratDasar;

        return $this;
    }

    public function getTanggalSuratDasar(): ?DateTimeInterface
    {
        return $this->tanggalSuratDasar;
    }

    public function setTanggalSuratDasar(DateTimeInterface $tanggalSuratDasar): self
    {
        $this->tanggalSuratDasar = $tanggalSuratDasar;

        return $this;
    }

    public function getKeteranganSuratDasar(): ?string
    {
        return $this->keteranganSuratDasar;
    }

    public function setKeteranganSuratDasar(?string $keteranganSuratDasar): self
    {
        $this->keteranganSuratDasar = $keteranganSuratDasar;

        return $this;
    }

    public function getIdPembuat()
    {
        return $this->idPembuat;
    }

    public function setIdPembuat($idPembuat): self
    {
        $this->idPembuat = $idPembuat;

        return $this;
    }

    public function getRolePembuat(): ?string
    {
        return $this->rolePembuat;
    }

    public function setRolePembuat(string $rolePembuat): self
    {
        $this->rolePembuat = $rolePembuat;

        return $this;
    }

    public function getTanggalBuat(): ?DateTimeInterface
    {
        return $this->tanggalBuat;
    }

    public function setTanggalBuat(DateTimeInterface $tanggalBuat): self
    {
        $this->tanggalBuat = $tanggalBuat;

        return $this;
    }

    public function getIdPengupdate()
    {
        return $this->idPengupdate;
    }

    public function setIdPengupdate($idPengupdate): self
    {
        $this->idPengupdate = $idPengupdate;

        return $this;
    }

    public function getRolePengupdate(): ?string
    {
        return $this->rolePengupdate;
    }

    public function setRolePengupdate(?string $rolePengupdate): self
    {
        $this->rolePengupdate = $rolePengupdate;

        return $this;
    }

    public function getTanggalUpdate(): ?DateTimeInterface
    {
        return $this->tanggalUpdate;
    }

    public function setTanggalUpdate(?DateTimeInterface $tanggalUpdate): self
    {
        $this->tanggalUpdate = $tanggalUpdate;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIdAtasanApprove()
    {
        return $this->idAtasanApprove;
    }

    public function setIdAtasanApprove($idAtasanApprove): self
    {
        $this->idAtasanApprove = $idAtasanApprove;

        return $this;
    }

    public function getTanggalAtasanApprove(): ?DateTimeInterface
    {
        return $this->tanggalAtasanApprove;
    }

    public function setTanggalAtasanApprove(?DateTimeInterface $tanggalAtasanApprove): self
    {
        $this->tanggalAtasanApprove = $tanggalAtasanApprove;

        return $this;
    }

    public function getIdKakapApprove()
    {
        return $this->idKakapApprove;
    }

    public function setIdKakapApprove($idKakapApprove): self
    {
        $this->idKakapApprove = $idKakapApprove;

        return $this;
    }

    public function getTanggalKakapApprove(): ?DateTimeInterface
    {
        return $this->tanggalKakapApprove;
    }

    public function setTanggalKakapApprove(?DateTimeInterface $tanggalKakapApprove): self
    {
        $this->tanggalKakapApprove = $tanggalKakapApprove;

        return $this;
    }

    public function getIdUpkPusatApprove()
    {
        return $this->idUpkPusatApprove;
    }

    public function setIdUpkPusatApprove($idUpkPusatApprove): self
    {
        $this->idUpkPusatApprove = $idUpkPusatApprove;

        return $this;
    }

    public function getTanggalUpkPusatApprove(): ?DateTimeInterface
    {
        return $this->tanggalUpkPusatApprove;
    }

    public function setTanggalUpkPusatApprove(?DateTimeInterface $tanggalUpkPusatApprove): self
    {
        $this->tanggalUpkPusatApprove = $tanggalUpkPusatApprove;

        return $this;
    }

    public function setNomorTicket(string $nomorTicket): self
    {
        $this->nomorTicket = $nomorTicket;

        return $this;
    }

    #[ORM\PrePersist]
    public function setNomorTicketValue(): void
    {
        $this->nomorTicket = 'LBR-'.AppHelper::RandomString(3).round(microtime(true));
    }

    public function getNomorTicket(): ?string
    {
        return $this->nomorTicket;
    }

    public function getHariLibur(): ?HariLibur
    {
        return $this->hariLibur;
    }

    public function setHariLibur(HariLibur $hariLibur): self
    {
        // set the owning side of the relation if necessary
        if ($hariLibur->getUsulan() !== $this) {
            $hariLibur->setUsulan($this);
        }

        $this->hariLibur = $hariLibur;

        return $this;
    }

    public function getUnitOrg()
    {
        return $this->unitOrg;
    }

    public function setUnitOrg($unitOrg): self
    {
        $this->unitOrg = $unitOrg;

        return $this;
    }

    public function getKantor()
    {
        return $this->kantor;
    }

    public function setKantor($kantor): self
    {
        $this->kantor = $kantor;

        return $this;
    }

    public function getKantorInduk()
    {
        return $this->kantorInduk;
    }

    public function setKantorInduk($kantorInduk): self
    {
        $this->kantorInduk = $kantorInduk;

        return $this;
    }
}
