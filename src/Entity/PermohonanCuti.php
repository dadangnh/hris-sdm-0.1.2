<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Helper\AppHelper;
use App\Repository\PermohonanCutiRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    normalizationContext: [
        'groups' => [
            'permohonanCuti:read'
        ],
        'swagger_definition_name' => 'read'
    ],
    denormalizationContext: [
        'groups' => [
            'permohonanCuti:write'
        ],
        'swagger_definition_name' => 'write'
    ],
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: PermohonanCutiRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_permohonan_cuti'
)]
#[ORM\Index(
    columns: [
        'id',
        'nomor_ticket',
        'tanggal_mulai',
        'tanggal_selesai',
        'pegawai_id',
        'kode_nomor',
        'nomor_surat',
        'tahun_surat'
    ],
    name: 'idx_permohonan_cuti'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nomorTicket' => 'exact',
        'nama' => 'ipartial',
        'pegawaiId' => 'exact',
        'nip9' => 'ipartial',
        'kantorId' => 'exact',
        'approvalAtasanLangsung' => 'exact',
        'approvalAtasanBerwenang' => 'exact',
        'status' => 'exact'
    ]
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggalMulai',
        'tanggalSelesai'
    ]
)]
class PermohonanCuti
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'string',
        length: 50
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nomorTicket;

    #[ORM\Column(
        type: 'string',
        length: 50
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nama;

    #[ORM\Column(
        type: 'uuid'
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $pegawaiId;

    #[ORM\Column(
        type: 'string',
        length: 18,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nip18;

    #[ORM\Column(
        type: 'string',
        length: 9,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nip9;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalMulai;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalSelesai;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?float $lamaCuti;

    #[ORM\Column(
        type: 'string',
        length: 128,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $kantor;

    #[ORM\Column(
        type: 'string',
        length: 128,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $unitOrganisasi;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $alasan;

    #[ORM\Column(
        type: 'string',
        length: 50,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $masaKerja;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $alamatSementara;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $persalinanKe;

    #[ORM\Column(
        type: 'string',
        length: 4
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $tahun;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $tempat;

    #[ORM\Column(
        type: 'string',
        length: 128,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $jabatan;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $atasanLangsung;

    #[ORM\Column(
        type: 'string',
        length: 18,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nipAtasanLangsung;

    #[ORM\Column(
        type: 'string',
        length: 128,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $namaAtasanLangsung;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $approvalAtasanLangsung;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $atasanBerwenang;

    #[ORM\Column(
        type: 'string',
        length: 18,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nipAtasanBerwenang;

    #[ORM\Column(
        type: 'string',
        length: 128,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $namaAtasanBerwenang;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $approvalAtasanBerwenang;

    #[ORM\Column(
        type: 'date'
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalBuat;

    #[ORM\Column(
        type: 'uuid'
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $namaBuat;

    #[ORM\Column(
        type: 'integer'
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $status;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $yth;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $tempatPejabat;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $sisaTahunLalu1;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $sisaTahunLalu2;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $sisaTahunIni;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nmPangkat;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalPersetujuan;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $namaSetuju;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $alasanPersetujuan;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglSrtIzinCuti;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $noSrtIzinCuti;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $alasanPembatalan;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?float $cutiDiambil;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $kantorId;

    #[ORM\Column(
        type: 'string',
        length: 100,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $noSketDokter;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $statusPerawatan;

    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $rumahSakit;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalPerawatanMulai;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalPerawatanSelesai;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $keguguran;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $ketDokter;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $statusAlasanPenting;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?float $sisaTotalMax;

    #[ORM\Column(
        type: 'float',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?float $lamaCutiBatal;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $statusCuti;

    #[ORM\Column(
        type: 'string',
        length: 100,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $noSuratBatal;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglSuratBatal;

    #[ORM\Column(
        type: 'string',
        length: 100,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $namaPejabatBatal;

    #[ORM\Column(
        type: 'string',
        length: 18,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $nipPejabatBatal;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalMulaiTugas;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tanggalSelesaiTugas;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $sampai;

    #[ORM\Column(
        type: 'string',
        length: 1,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $jnsSt;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $jumlahCutiDitangguhkanLt;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $jumlahCutiLt;

    #[ORM\Column(
        type: 'string',
        length: 4,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $tahunLt;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglBerangkatHaji;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?DateTimeInterface $tglKembaliHaji;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $tipeCutiHalf;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?int $kebenaranUpk;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $kdDati2;

    #[ORM\OneToMany(
        mappedBy: 'permohonanCuti',
        targetEntity: CutiPegawai::class,
        orphanRemoval: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private Collection $cutiPegawai;

    #[ORM\Column(
        type: 'string',
        length: 20,
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $phone;

    #[ORM\ManyToOne(
        targetEntity: JnsCuti::class
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?JnsCuti $jnsCuti;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private ?string $catatan;

    #[ORM\Column(
        type: 'string',
        length: 7,
        nullable: true
    )]
    private ?string $kodeNomor;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $nomorSurat;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $tahunSurat;

    #[ORM\OneToOne(mappedBy: 'permohonanCuti', targetEntity: PermohonanCutiTambahan::class, cascade: ['persist', 'remove'])]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCuti:write'
        ]
    )]
    private $permohonanCutiTambahan;

    public function __construct()
    {
        $this->id           = Uuid::v6();
        $this->cutiPegawai  = new ArrayCollection();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function generatedNoSI(LifecycleEventArgs $event): void
    {
        $entityManager  = $event->getObjectManager();
        $repository     = $entityManager->getRepository(get_class($this));

        if (
            null === $this->getNoSrtIzinCuti()
            && 1 === $this->getStatus()
            && 1 === $this->getApprovalAtasanLangsung()
            && null === $this->getApprovalAtasanBerwenang()
        ) {
            $this->tahunSurat = date('Y');

            if (in_array($this->getJnsCuti()?->getJnsCuti(), array(6, 9, 10), true)) {
                $this->kodeNomor = 'SICT-';
            }

            if (in_array($this->getJnsCuti()?->getJnsCuti(), array(2, 7, 11), true)) {
                $this->kodeNomor = 'SICS-';
            }

            if (1 === $this->getJnsCuti()?->getJnsCuti()) {
                $this->kodeNomor = 'SICB-';
            }

            if (3 === $this->getJnsCuti()?->getJnsCuti()) {
                $this->kodeNomor = 'SICM-';
            }

            if (4 === $this->getJnsCuti()?->getJnsCuti()) {
                $this->kodeNomor = 'SICKAP-';
            }

            if (5 === $this->getJnsCuti()?->getJnsCuti()) {
                $this->kodeNomor = 'SICLTN-';
            }

            $nomorSurat         = $repository->getMaxNomorSurat($this->kodeNomor, $this->tahunSurat);
            $this->nomorSurat   = $nomorSurat[0]['nomor_surat'];

            $this->noSrtIzinCuti    = $this->kodeNomor . $this->nomorSurat . '/PJ/' . $this->tahunSurat;
            $this->tglSrtIzinCuti   = new DateTime();
        }
    }

    public function createPenundaanForCLT(ManagerRegistry $doctrine): void
    {
        $cutiPegawai = $this->getCutiPegawai();

        if(2 === $this->getStatus() && 9 === $this->getJnsCuti()?->getJnsCuti() && 0 === count($cutiPegawai)){
            $riwayatCuti = new CutiPegawai();
            $riwayatCuti->setNomorTicket($this->getNomorTicket());
            $riwayatCuti->setJnsCuti($this->getJnsCuti());
            $riwayatCuti->setPermohonanCuti($this);
            $riwayatCuti->setPegawaiId($this->getPegawaiId());
            $riwayatCuti->setNip9($this->getNip9());
            $riwayatCuti->setTglSrtIzinCuti($this->getTglSrtIzinCuti());
            $riwayatCuti->setNoSketDokter($this->getNoSrtIzinCuti());
            $riwayatCuti->setTglAwalCutiPeg($this->getTanggalMulai());
            $riwayatCuti->setTglAkhirCutiPeg($this->getTanggalSelesai());
            $riwayatCuti->setSisaCuti($this->getJumlahCutiDitangguhkanLt());
            $riwayatCuti->setTahun($this->getTahun());
            $riwayatCuti->setNamaBuat($this->getNamaBuat());
            $riwayatCuti->setTanggalBuat($this->getTanggalBuat());

            $doctrine->getManager()->persist($riwayatCuti);
            $doctrine->getManager()->flush();
        }
    }

    public function getNomorTicket(): ?string
    {
        return $this->nomorTicket;
    }

    public function setNomorTicket(string $nomorTicket): self
    {
        $this->nomorTicket = $nomorTicket;

        return $this;
    }

    /**
     * @throws Exception
     */
    #[ORM\PrePersist]
    public function setNomorTicketValue(): void
    {
        $this->nomorTicket = 'PCP-' . AppHelper::RandomString(3) . round(microtime(true));
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getPegawaiId()
    {
        return $this->pegawaiId;
    }

    public function setPegawaiId($pegawaiId): self
    {
        $this->pegawaiId = $pegawaiId;

        return $this;
    }

    public function getNip18(): ?string
    {
        return $this->nip18;
    }

    public function setNip18(?string $nip18): self
    {
        $this->nip18 = $nip18;

        return $this;
    }

    public function getNip9(): ?string
    {
        return $this->nip9;
    }

    public function setNip9(?string $nip9): self
    {
        $this->nip9 = $nip9;

        return $this;
    }

    public function getTanggalMulai(): ?DateTimeInterface
    {
        return $this->tanggalMulai;
    }

    public function setTanggalMulai(?DateTimeInterface $tanggalMulai): self
    {
        $this->tanggalMulai = $tanggalMulai;

        return $this;
    }

    public function getTanggalSelesai(): ?DateTimeInterface
    {
        return $this->tanggalSelesai;
    }

    public function setTanggalSelesai(?DateTimeInterface $tanggalSelesai): self
    {
        $this->tanggalSelesai = $tanggalSelesai;

        return $this;
    }

    public function getLamaCuti(): ?float
    {
        return $this->lamaCuti;
    }

    public function setLamaCuti(?float $lamaCuti): self
    {
        $this->lamaCuti = $lamaCuti;

        return $this;
    }

    public function getKantor(): ?string
    {
        return $this->kantor;
    }

    public function setKantor(?string $kantor): self
    {
        $this->kantor = $kantor;

        return $this;
    }

    public function getUnitOrganisasi(): ?string
    {
        return $this->unitOrganisasi;
    }

    public function setUnitOrganisasi(?string $unitOrganisasi): self
    {
        $this->unitOrganisasi = $unitOrganisasi;

        return $this;
    }

    public function getAlasan(): ?string
    {
        return $this->alasan;
    }

    public function setAlasan(?string $alasan): self
    {
        $this->alasan = $alasan;

        return $this;
    }

    public function getMasaKerja(): ?string
    {
        return $this->masaKerja;
    }

    public function setMasaKerja(?string $masaKerja): self
    {
        $this->masaKerja = $masaKerja;

        return $this;
    }

    public function getAlamatSementara(): ?string
    {
        return $this->alamatSementara;
    }

    public function setAlamatSementara(?string $alamatSementara): self
    {
        $this->alamatSementara = $alamatSementara;

        return $this;
    }

    public function getPersalinanKe(): ?int
    {
        return $this->persalinanKe;
    }

    public function setPersalinanKe(?int $persalinanKe): self
    {
        $this->persalinanKe = $persalinanKe;

        return $this;
    }

    public function getTahun(): ?string
    {
        return $this->tahun;
    }

    public function setTahun(string $tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    public function getTempat(): ?string
    {
        return $this->tempat;
    }

    public function setTempat(?string $tempat): self
    {
        $this->tempat = $tempat;

        return $this;
    }

    public function getJabatan(): ?string
    {
        return $this->jabatan;
    }

    public function setJabatan(?string $jabatan): self
    {
        $this->jabatan = $jabatan;

        return $this;
    }

    public function getAtasanLangsung()
    {
        return $this->atasanLangsung;
    }

    public function setAtasanLangsung($atasanLangsung): self
    {
        $this->atasanLangsung = $atasanLangsung;

        return $this;
    }

    public function getNipAtasanLangsung(): ?string
    {
        return $this->nipAtasanLangsung;
    }

    public function setNipAtasanLangsung(?string $nipAtasanLangsung): self
    {
        $this->nipAtasanLangsung = $nipAtasanLangsung;

        return $this;
    }

    public function getNamaAtasanLangsung(): ?string
    {
        return $this->namaAtasanLangsung;
    }

    public function setNamaAtasanLangsung(?string $namaAtasanLangsung): self
    {
        $this->namaAtasanLangsung = $namaAtasanLangsung;

        return $this;
    }

    public function getApprovalAtasanLangsung(): ?int
    {
        return $this->approvalAtasanLangsung;
    }

    public function setApprovalAtasanLangsung(?int $approvalAtasanLangsung): self
    {
        $this->approvalAtasanLangsung = $approvalAtasanLangsung;

        return $this;
    }

    public function getAtasanBerwenang()
    {
        return $this->atasanBerwenang;
    }

    public function setAtasanBerwenang($atasanBerwenang): self
    {
        $this->atasanBerwenang = $atasanBerwenang;

        return $this;
    }

    public function getNipAtasanBerwenang(): ?string
    {
        return $this->nipAtasanBerwenang;
    }

    public function setNipAtasanBerwenang(?string $nipAtasanBerwenang): self
    {
        $this->nipAtasanBerwenang = $nipAtasanBerwenang;

        return $this;
    }

    public function getNamaAtasanBerwenang(): ?string
    {
        return $this->namaAtasanBerwenang;
    }

    public function setNamaAtasanBerwenang(?string $namaAtasanBerwenang): self
    {
        $this->namaAtasanBerwenang = $namaAtasanBerwenang;

        return $this;
    }

    public function getApprovalAtasanBerwenang(): ?int
    {
        return $this->approvalAtasanBerwenang;
    }

    public function setApprovalAtasanBerwenang(?int $approvalAtasanBerwenang): self
    {
        $this->approvalAtasanBerwenang = $approvalAtasanBerwenang;

        return $this;
    }

    public function getTanggalBuat(): ?DateTimeInterface
    {
        return $this->tanggalBuat;
    }

    public function setTanggalBuat(DateTimeInterface $tanggalBuat): self
    {
        $this->tanggalBuat = $tanggalBuat;

        return $this;
    }

    public function getNamaBuat()
    {
        return $this->namaBuat;
    }

    public function setNamaBuat($namaBuat): self
    {
        $this->namaBuat = $namaBuat;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getYth(): ?string
    {
        return $this->yth;
    }

    public function setYth(?string $yth): self
    {
        $this->yth = $yth;

        return $this;
    }

    public function getTempatPejabat(): ?string
    {
        return $this->tempatPejabat;
    }

    public function setTempatPejabat(?string $tempatPejabat): self
    {
        $this->tempatPejabat = $tempatPejabat;

        return $this;
    }

    public function getSisaTahunLalu1(): ?int
    {
        return $this->sisaTahunLalu1;
    }

    public function setSisaTahunLalu1(?int $sisaTahunLalu1): self
    {
        $this->sisaTahunLalu1 = $sisaTahunLalu1;

        return $this;
    }

    public function getSisaTahunLalu2(): ?int
    {
        return $this->sisaTahunLalu2;
    }

    public function setSisaTahunLalu2(?int $sisaTahunLalu2): self
    {
        $this->sisaTahunLalu2 = $sisaTahunLalu2;

        return $this;
    }

    public function getSisaTahunIni(): ?int
    {
        return $this->sisaTahunIni;
    }

    public function setSisaTahunIni(?int $sisaTahunIni): self
    {
        $this->sisaTahunIni = $sisaTahunIni;

        return $this;
    }

    public function getNmPangkat(): ?string
    {
        return $this->nmPangkat;
    }

    public function setNmPangkat(?string $nmPangkat): self
    {
        $this->nmPangkat = $nmPangkat;

        return $this;
    }

    public function getTanggalPersetujuan(): ?DateTimeInterface
    {
        return $this->tanggalPersetujuan;
    }

    public function setTanggalPersetujuan(?DateTimeInterface $tanggalPersetujuan): self
    {
        $this->tanggalPersetujuan = $tanggalPersetujuan;

        return $this;
    }

    public function getNamaSetuju()
    {
        return $this->namaSetuju;
    }

    public function setNamaSetuju($namaSetuju): self
    {
        $this->namaSetuju = $namaSetuju;

        return $this;
    }

    public function getAlasanPersetujuan(): ?string
    {
        return $this->alasanPersetujuan;
    }

    public function setAlasanPersetujuan(?string $alasanPersetujuan): self
    {
        $this->alasanPersetujuan = $alasanPersetujuan;

        return $this;
    }

    public function getTglSrtIzinCuti(): ?DateTimeInterface
    {
        return $this->tglSrtIzinCuti;
    }

    public function setTglSrtIzinCuti(?DateTimeInterface $tglSrtIzinCuti): self
    {
        $this->tglSrtIzinCuti = $tglSrtIzinCuti;

        return $this;
    }

    public function getNoSrtIzinCuti(): ?string
    {
        return $this->noSrtIzinCuti;
    }

    public function setNoSrtIzinCuti(?string $noSrtIzinCuti): self
    {
        $this->noSrtIzinCuti = $noSrtIzinCuti;

        return $this;
    }

    public function getAlasanPembatalan(): ?string
    {
        return $this->alasanPembatalan;
    }

    public function setAlasanPembatalan(?string $alasanPembatalan): self
    {
        $this->alasanPembatalan = $alasanPembatalan;

        return $this;
    }

    public function getCutiDiambil(): ?float
    {
        return $this->cutiDiambil;
    }

    public function setCutiDiambil(?float $cutiDiambil): self
    {
        $this->cutiDiambil = $cutiDiambil;

        return $this;
    }

    public function getKantorId()
    {
        return $this->kantorId;
    }

    public function setKantorId($kantorId): self
    {
        $this->kantorId = $kantorId;

        return $this;
    }

    public function getNoSketDokter(): ?string
    {
        return $this->noSketDokter;
    }

    public function setNoSketDokter(?string $noSketDokter): self
    {
        $this->noSketDokter = $noSketDokter;

        return $this;
    }

    public function getStatusPerawatan(): ?string
    {
        return $this->statusPerawatan;
    }

    public function setStatusPerawatan(?string $statusPerawatan): self
    {
        $this->statusPerawatan = $statusPerawatan;

        return $this;
    }

    public function getRumahSakit(): ?string
    {
        return $this->rumahSakit;
    }

    public function setRumahSakit(?string $rumahSakit): self
    {
        $this->rumahSakit = $rumahSakit;

        return $this;
    }

    public function getTanggalPerawatanMulai(): ?DateTimeInterface
    {
        return $this->tanggalPerawatanMulai;
    }

    public function setTanggalPerawatanMulai(?DateTimeInterface $tanggalPerawatanMulai): self
    {
        $this->tanggalPerawatanMulai = $tanggalPerawatanMulai;

        return $this;
    }

    public function getTanggalPerawatanSelesai(): ?DateTimeInterface
    {
        return $this->tanggalPerawatanSelesai;
    }

    public function setTanggalPerawatanSelesai(?DateTimeInterface $tanggalPerawatanSelesai): self
    {
        $this->tanggalPerawatanSelesai = $tanggalPerawatanSelesai;

        return $this;
    }

    public function getKeguguran(): ?string
    {
        return $this->keguguran;
    }

    public function setKeguguran(?string $keguguran): self
    {
        $this->keguguran = $keguguran;

        return $this;
    }

    public function getKetDokter(): ?string
    {
        return $this->ketDokter;
    }

    public function setKetDokter(?string $ketDokter): self
    {
        $this->ketDokter = $ketDokter;

        return $this;
    }

    public function getStatusAlasanPenting(): ?string
    {
        return $this->statusAlasanPenting;
    }

    public function setStatusAlasanPenting(?string $statusAlasanPenting): self
    {
        $this->statusAlasanPenting = $statusAlasanPenting;

        return $this;
    }

    public function getSisaTotalMax(): ?float
    {
        return $this->sisaTotalMax;
    }

    public function setSisaTotalMax(?float $sisaTotalMax): self
    {
        $this->sisaTotalMax = $sisaTotalMax;

        return $this;
    }

    public function getLamaCutiBatal(): ?float
    {
        return $this->lamaCutiBatal;
    }

    public function setLamaCutiBatal(?float $lamaCutiBatal): self
    {
        $this->lamaCutiBatal = $lamaCutiBatal;

        return $this;
    }

    public function getStatusCuti(): ?string
    {
        return $this->statusCuti;
    }

    public function setStatusCuti(?string $statusCuti): self
    {
        $this->statusCuti = $statusCuti;

        return $this;
    }

    public function getNoSuratBatal(): ?string
    {
        return $this->noSuratBatal;
    }

    public function setNoSuratBatal(?string $noSuratBatal): self
    {
        $this->noSuratBatal = $noSuratBatal;

        return $this;
    }

    public function getTglSuratBatal(): ?DateTimeInterface
    {
        return $this->tglSuratBatal;
    }

    public function setTglSuratBatal(?DateTimeInterface $tglSuratBatal): self
    {
        $this->tglSuratBatal = $tglSuratBatal;

        return $this;
    }

    public function getNamaPejabatBatal(): ?string
    {
        return $this->namaPejabatBatal;
    }

    public function setNamaPejabatBatal(?string $namaPejabatBatal): self
    {
        $this->namaPejabatBatal = $namaPejabatBatal;

        return $this;
    }

    public function getNipPejabatBatal(): ?string
    {
        return $this->nipPejabatBatal;
    }

    public function setNipPejabatBatal(?string $nipPejabatBatal): self
    {
        $this->nipPejabatBatal = $nipPejabatBatal;

        return $this;
    }

    public function getTanggalMulaiTugas(): ?DateTimeInterface
    {
        return $this->tanggalMulaiTugas;
    }

    public function setTanggalMulaiTugas(?DateTimeInterface $tanggalMulaiTugas): self
    {
        $this->tanggalMulaiTugas = $tanggalMulaiTugas;

        return $this;
    }

    public function getTanggalSelesaiTugas(): ?DateTimeInterface
    {
        return $this->tanggalSelesaiTugas;
    }

    public function setTanggalSelesaiTugas(?DateTimeInterface $tanggalSelesaiTugas): self
    {
        $this->tanggalSelesaiTugas = $tanggalSelesaiTugas;

        return $this;
    }

    public function getSampai(): ?string
    {
        return $this->sampai;
    }

    public function setSampai(?string $sampai): self
    {
        $this->sampai = $sampai;

        return $this;
    }

    public function getJnsSt(): ?string
    {
        return $this->jnsSt;
    }

    public function setJnsSt(?string $jnsSt): self
    {
        $this->jnsSt = $jnsSt;

        return $this;
    }

    public function getJumlahCutiDitangguhkanLt(): ?int
    {
        return $this->jumlahCutiDitangguhkanLt;
    }

    public function setJumlahCutiDitangguhkanLt(?int $jumlahCutiDitangguhkanLt): self
    {
        $this->jumlahCutiDitangguhkanLt = $jumlahCutiDitangguhkanLt;

        return $this;
    }

    public function getJumlahCutiLt(): ?int
    {
        return $this->jumlahCutiLt;
    }

    public function setJumlahCutiLt(?int $jumlahCutiLt): self
    {
        $this->jumlahCutiLt = $jumlahCutiLt;

        return $this;
    }

    public function getTahunLt(): ?string
    {
        return $this->tahunLt;
    }

    public function setTahunLt(?string $tahunLt): self
    {
        $this->tahunLt = $tahunLt;

        return $this;
    }

    public function getTglBerangkatHaji(): ?DateTimeInterface
    {
        return $this->tglBerangkatHaji;
    }

    public function setTglBerangkatHaji(?DateTimeInterface $tglBerangkatHaji): self
    {
        $this->tglBerangkatHaji = $tglBerangkatHaji;

        return $this;
    }

    public function getTglKembaliHaji(): ?DateTimeInterface
    {
        return $this->tglKembaliHaji;
    }

    public function setTglKembaliHaji(?DateTimeInterface $tglKembaliHaji): self
    {
        $this->tglKembaliHaji = $tglKembaliHaji;

        return $this;
    }

    public function getTipeCutiHalf(): ?int
    {
        return $this->tipeCutiHalf;
    }

    public function setTipeCutiHalf(?int $tipeCutiHalf): self
    {
        $this->tipeCutiHalf = $tipeCutiHalf;

        return $this;
    }

    public function getKebenaranUpk(): ?int
    {
        return $this->kebenaranUpk;
    }

    public function setKebenaranUpk(?int $kebenaranUpk): self
    {
        $this->kebenaranUpk = $kebenaranUpk;

        return $this;
    }

    public function getKdDati2()
    {
        return $this->kdDati2;
    }

    public function setKdDati2($kdDati2): self
    {
        $this->kdDati2 = $kdDati2;

        return $this;
    }

    /**
     * @return Collection|CutiPegawai[]
     */
    public function getCutiPegawai(): Collection|array
    {
        return $this->cutiPegawai;
    }

    public function addCutiPegawai(CutiPegawai $cutiPegawai): self
    {
        if (!$this->cutiPegawai->contains($cutiPegawai)) {
            $this->cutiPegawai[] = $cutiPegawai;
        }

        return $this;
    }

    public function removeCutiPegawai(CutiPegawai $cutiPegawai): self
    {
        if ($this->cutiPegawai->contains($cutiPegawai)) {
            $this->cutiPegawai->removeElement($cutiPegawai);
        }
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getJnsCuti(): ?JnsCuti
    {
        return $this->jnsCuti;
    }

    public function setJnsCuti(?JnsCuti $jnsCuti): self
    {
        $this->jnsCuti = $jnsCuti;

        return $this;
    }

    public function getCatatan(): ?string
    {
        return $this->catatan;
    }

    public function setCatatan(?string $catatan): self
    {
        $this->catatan = $catatan;

        return $this;
    }

    public function getKodeNomor(): ?string
    {
        return $this->kodeNomor;
    }

    public function setKodeNomor(?string $kodeNomor): self
    {
        $this->kodeNomor = $kodeNomor;

        return $this;
    }

    public function getNomorSurat(): ?int
    {
        return $this->nomorSurat;
    }

    public function setNomorSurat(?int $nomorSurat): self
    {
        $this->nomorSurat = $nomorSurat;

        return $this;
    }

    public function getTahunSurat(): ?int
    {
        return $this->tahunSurat;
    }

    public function setTahunSurat(?int $tahunSurat): self
    {
        $this->tahunSurat = $tahunSurat;

        return $this;
    }

    public function getPermohonanCutiTambahan(): ?PermohonanCutiTambahan
    {
        return $this->permohonanCutiTambahan;
    }

    public function setPermohonanCutiTambahan(PermohonanCutiTambahan $permohonanCutiTambahan): self
    {
        // set the owning side of the relation if necessary
        if ($permohonanCutiTambahan->getPermohonanCuti() !== $this) {
            $permohonanCutiTambahan->setPermohonanCuti($this);
        }

        $this->permohonanCutiTambahan = $permohonanCutiTambahan;

        return $this;
    }
}
