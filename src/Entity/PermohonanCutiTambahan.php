<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\PermohonanCutiTambahanRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    normalizationContext: [
        'groups' => [
            'permohonanCutiTambahan:read'
        ],
        'swagger_definition_name' => 'read'
    ],
    denormalizationContext: [
        'groups' => [
            'permohonanCutiTambahan:write'
        ],
        'swagger_definition_name' => 'write'
    ],
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: PermohonanCutiTambahanRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_permohonan_cuti_tambahan'
)]
#[ORM\Index(
    columns: [
        'id',
        'permohonan_cuti_id',
        'jenis_id',
        'tanggal_mulai',
        'tanggal_selesai'
    ],
    name: 'idx_permohonan_cuti_tambahan'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'jenis' => 'exact',
        'status' => 'exact',
        'permohonanCuti' => 'exact',
        'pegawaiId' => 'exact']
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggalMulai',
        'tanggalSelesai'
    ]
)]
class PermohonanCutiTambahan
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[Groups(
        groups: [
            'permohonanCuti:read',
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private UuidV6 $id;

    #[ORM\Column(type: 'date')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?DateTimeInterface $tanggalMulai;

    #[ORM\Column(type: 'date')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?DateTimeInterface $tanggalSelesai;

    #[ORM\Column(type: 'float')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?float $lamaCutiTambahan;

    #[ORM\Column(type: 'text')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?string $alasanCutiTambahan;

    #[ORM\Column(type: 'integer')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?int $status;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?int $approvalAtasanLangsung;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?int $approvalAtasanBerwenang;

    #[ORM\Column(type: 'uuid')]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private $pegawaiId;

    #[ORM\Column(type: 'uuid', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private $atasanLangsung;

    #[ORM\Column(type: 'string', length: 18, nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?string $nipAtasanLangsung;

    #[ORM\Column(type: 'string', length: 128, nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?string $namaAtasanLangsung;

    #[ORM\Column(type: 'uuid', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private $atasanBerwenang;

    #[ORM\Column(type: 'string', length: 18, nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?string $nipAtasanBerwenang;

    #[ORM\Column(type: 'string', length: 128, nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?string $namaAtasanBerwenang;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?DateTimeInterface $tanggalPersetujuan;

    #[ORM\Column(type: 'uuid', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private $namaSetuju;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?string $alasanPembatalan;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?int $batal;

    #[ORM\OneToOne(inversedBy: 'permohonanCutiTambahan', targetEntity: PermohonanCuti::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?PermohonanCuti $permohonanCuti;

    #[ORM\ManyToOne(targetEntity: JnsCutiTambahan::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private ?JnsCutiTambahan $jenis;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private $kantor;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(
        groups: [
            'permohonanCutiTambahan:read',
            'permohonanCutiTambahan:write'
        ]
    )]
    private $unitOrganisasi;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getTanggalMulai(): ?DateTimeInterface
    {
        return $this->tanggalMulai;
    }

    public function setTanggalMulai(DateTimeInterface $tanggalMulai): self
    {
        $this->tanggalMulai = $tanggalMulai;

        return $this;
    }

    public function getTanggalSelesai(): ?DateTimeInterface
    {
        return $this->tanggalSelesai;
    }

    public function setTanggalSelesai(DateTimeInterface $tanggalSelesai): self
    {
        $this->tanggalSelesai = $tanggalSelesai;

        return $this;
    }

    public function getLamaCutiTambahan(): ?float
    {
        return $this->lamaCutiTambahan;
    }

    public function setLamaCutiTambahan(float $lamaCutiTambahan): self
    {
        $this->lamaCutiTambahan = $lamaCutiTambahan;

        return $this;
    }

    public function getAlasanCutiTambahan(): ?string
    {
        return $this->alasanCutiTambahan;
    }

    public function setAlasanCutiTambahan(string $alasanCutiTambahan): self
    {
        $this->alasanCutiTambahan = $alasanCutiTambahan;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getApprovalAtasanLangsung(): ?int
    {
        return $this->approvalAtasanLangsung;
    }

    public function setApprovalAtasanLangsung(?int $approvalAtasanLangsung): self
    {
        $this->approvalAtasanLangsung = $approvalAtasanLangsung;

        return $this;
    }

    public function getApprovalAtasanBerwenang(): ?int
    {
        return $this->approvalAtasanBerwenang;
    }

    public function setApprovalAtasanBerwenang(?int $approvalAtasanBerwenang): self
    {
        $this->approvalAtasanBerwenang = $approvalAtasanBerwenang;

        return $this;
    }

    public function getPegawaiId()
    {
        return $this->pegawaiId;
    }

    public function setPegawaiId($pegawaiId): self
    {
        $this->pegawaiId = $pegawaiId;

        return $this;
    }

    public function getAtasanLangsung()
    {
        return $this->atasanLangsung;
    }

    public function setAtasanLangsung($atasanLangsung): self
    {
        $this->atasanLangsung = $atasanLangsung;

        return $this;
    }

    public function getNipAtasanLangsung(): ?string
    {
        return $this->nipAtasanLangsung;
    }

    public function setNipAtasanLangsung(?string $nipAtasanLangsung): self
    {
        $this->nipAtasanLangsung = $nipAtasanLangsung;

        return $this;
    }

    public function getNamaAtasanLangsung(): ?string
    {
        return $this->namaAtasanLangsung;
    }

    public function setNamaAtasanLangsung(?string $namaAtasanLangsung): self
    {
        $this->namaAtasanLangsung = $namaAtasanLangsung;

        return $this;
    }

    public function getAtasanBerwenang()
    {
        return $this->atasanBerwenang;
    }

    public function setAtasanBerwenang($atasanBerwenang): self
    {
        $this->atasanBerwenang = $atasanBerwenang;

        return $this;
    }

    public function getNipAtasanBerwenang(): ?string
    {
        return $this->nipAtasanBerwenang;
    }

    public function setNipAtasanBerwenang(?string $nipAtasanBerwenang): self
    {
        $this->nipAtasanBerwenang = $nipAtasanBerwenang;

        return $this;
    }

    public function getNamaAtasanBerwenang(): ?string
    {
        return $this->namaAtasanBerwenang;
    }

    public function setNamaAtasanBerwenang(?string $namaAtasanBerwenang): self
    {
        $this->namaAtasanBerwenang = $namaAtasanBerwenang;

        return $this;
    }

    public function getTanggalPersetujuan(): ?DateTimeInterface
    {
        return $this->tanggalPersetujuan;
    }

    public function setTanggalPersetujuan(?DateTimeInterface $tanggalPersetujuan): self
    {
        $this->tanggalPersetujuan = $tanggalPersetujuan;

        return $this;
    }

    public function getNamaSetuju()
    {
        return $this->namaSetuju;
    }

    public function setNamaSetuju($namaSetuju): self
    {
        $this->namaSetuju = $namaSetuju;

        return $this;
    }

    public function getAlasanPembatalan(): ?string
    {
        return $this->alasanPembatalan;
    }

    public function setAlasanPembatalan(?string $alasanPembatalan): self
    {
        $this->alasanPembatalan = $alasanPembatalan;

        return $this;
    }

    public function getBatal(): ?int
    {
        return $this->batal;
    }

    public function setBatal(?int $batal): self
    {
        $this->batal = $batal;

        return $this;
    }

    public function getPermohonanCuti(): ?PermohonanCuti
    {
        return $this->permohonanCuti;
    }

    public function setPermohonanCuti(PermohonanCuti $permohonanCuti): self
    {
        $this->permohonanCuti = $permohonanCuti;

        return $this;
    }

    public function getJenis(): ?JnsCutiTambahan
    {
        return $this->jenis;
    }

    public function setJenis(?JnsCutiTambahan $jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }

    public function getKantor(): ?string
    {
        return $this->kantor;
    }

    public function setKantor(?string $kantor): self
    {
        $this->kantor = $kantor;

        return $this;
    }

    public function getUnitOrganisasi(): ?string
    {
        return $this->unitOrganisasi;
    }

    public function setUnitOrganisasi(?string $unitOrganisasi): self
    {
        $this->unitOrganisasi = $unitOrganisasi;

        return $this;
    }
}
