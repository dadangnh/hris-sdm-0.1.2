<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Helper\AppHelper;
use App\Repository\CutiBersamaRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'tanggalMulai' => 'DESC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: CutiBersamaRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_cuti_bersama'
)]
#[ORM\Index(
    columns: [
        'id',
        'nomor_ticket',
        'tanggal_mulai',
        'tanggal_selesai'
    ],
    name: 'idx_cuti_bersama'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nomorTicket' => 'ipartial',
        'keterangan' => 'ipartial'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: ['status']
)]
#[ApiFilter(
    filterClass: DateFilter::class,
    properties: [
        'tanggalMulai',
        'tanggalSelesai'
    ]
)]
class CutiBersama
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'string',
        length: 50
    )]
    private ?string $nomorTicket;

    #[ORM\Column(
        type: 'date'
    )]
    private ?DateTimeInterface $tanggalMulai;

    #[ORM\Column(
        type: 'date'
    )]
    private ?DateTimeInterface $tanggalSelesai;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    private ?DateTimeInterface $dateCreated;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private $createdBy;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    private ?DateTimeInterface $dateApproved;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private $approvedBy;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $status;

    #[ORM\Column(
        type: 'text',
        nullable: true
    )]
    private ?string $keterangan;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $reduce;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getNomorTicket(): ?string
    {
        return $this->nomorTicket;
    }

    public function setNomorTicket(string $nomorTicket): self
    {
        $this->nomorTicket = $nomorTicket;
        return $this;
    }

    /**
     * @throws Exception
     */
    #[ORM\PrePersist]
    public function setNomorTicketValue(): void
    {
        $this->nomorTicket = 'LCB-' . AppHelper::RandomString(3) . round(microtime(true));
    }

    public function getTanggalMulai(): ?DateTimeInterface
    {
        return $this->tanggalMulai;
    }

    public function setTanggalMulai(DateTimeInterface $tanggalMulai): self
    {
        $this->tanggalMulai = $tanggalMulai;
        return $this;
    }

    public function getTanggalSelesai(): ?DateTimeInterface
    {
        return $this->tanggalSelesai;
    }

    public function setTanggalSelesai(DateTimeInterface $tanggalSelesai): self
    {
        $this->tanggalSelesai = $tanggalSelesai;
        return $this;
    }

    public function getDateCreated(): ?DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy): self
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    public function getDateApproved(): ?DateTimeInterface
    {
        return $this->dateApproved;
    }
    public function setDateApproved(?DateTimeInterface $dateApproved) : self
    {
        $this->dateApproved = $dateApproved;
        return $this;
    }

    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    public function setApprovedBy($approvedBy): self
    {
        $this->approvedBy = $approvedBy;
        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getKeterangan(): ?string
    {
        return $this->keterangan;
    }

    public function setKeterangan(?string $keterangan): self
    {
        $this->keterangan = $keterangan;
        return $this;
    }

    public function getReduce(): ?int
    {
        return $this->reduce;
    }

    public function setReduce(?int $reduce): self
    {
        $this->reduce = $reduce;
        return $this;
    }
}
