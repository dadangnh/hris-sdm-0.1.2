<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\CutiPegawaiStatusRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;

#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'nip9' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: CutiPegawaiStatusRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_cuti_pegawai_status'
)]
#[ORM\Index(
    columns: [
        'id',
        'nip9',
        'pegawai_id'
    ],
    name: 'idx_cuti_pegawai_status'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'nip9' => 'ipartial',
        'pegawaiId' => 'exact'
    ]
)]
class CutiPegawaiStatus
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    private UuidV6 $id;

    #[ORM\Column(
        type: 'string',
        length: 9,
        nullable: true
    )]
    private ?string $nip9;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private $pegawaiId;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $hakCuti;

    #[ORM\Column(
        type: 'string',
        length: 3
    )]
    private ?string $kdStatusKepeg;

    #[ORM\Column(
        type: 'date'
    )]
    private ?DateTimeInterface $tmtCpns;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    private ?DateTimeInterface $cutiBesarTerakhir;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $countCutiBesar;

    #[ORM\Column(
        type: 'date',
        nullable: true
    )]
    private ?DateTimeInterface $cltnTerakhir;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $countCltn;

    #[ORM\Column(
        type: 'integer',
        nullable: true
    )]
    private ?int $lamaCltn;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }

    public function getId(): UuidV6
    {
        return $this->id;
    }

    public function getNip9(): ?string
    {
        return $this->nip9;
    }

    public function setNip9(?string $nip9): self
    {
        $this->nip9 = $nip9;

        return $this;
    }

    public function getPegawaiId()
    {
        return $this->pegawaiId;
    }

    public function setPegawaiId($pegawaiId): self
    {
        $this->pegawaiId = $pegawaiId;

        return $this;
    }

    public function getHakCuti(): ?int
    {
        return $this->hakCuti;
    }

    public function setHakCuti(?int $hakCuti): self
    {
        $this->hakCuti = $hakCuti;

        return $this;
    }

    public function getKdStatusKepeg(): ?string
    {
        return $this->kdStatusKepeg;
    }

    public function setKdStatusKepeg(string $kdStatusKepeg): self
    {
        $this->kdStatusKepeg = $kdStatusKepeg;

        return $this;
    }

    public function getTmtCpns(): ?DateTimeInterface
    {
        return $this->tmtCpns;
    }

    public function setTmtCpns(DateTimeInterface $tmtCpns): self
    {
        $this->tmtCpns = $tmtCpns;

        return $this;
    }

    public function getCutiBesarTerakhir(): ?DateTimeInterface
    {
        return $this->cutiBesarTerakhir;
    }

    public function setCutiBesarTerakhir(?DateTimeInterface $cutiBesarTerakhir): self
    {
        $this->cutiBesarTerakhir = $cutiBesarTerakhir;

        return $this;
    }

    public function getCountCutiBesar(): ?int
    {
        return $this->countCutiBesar;
    }

    public function setCountCutiBesar(?int $countCutiBesar): self
    {
        $this->countCutiBesar = $countCutiBesar;

        return $this;
    }

    public function getCltnTerakhir(): ?DateTimeInterface
    {
        return $this->cltnTerakhir;
    }

    public function setCltnTerakhir(?DateTimeInterface $cltnTerakhir): self
    {
        $this->cltnTerakhir = $cltnTerakhir;

        return $this;
    }

    public function getCountCltn(): ?int
    {
        return $this->countCltn;
    }

    public function setCountCltn(?int $countCltn): self
    {
        $this->countCltn = $countCltn;

        return $this;
    }

    public function getLamaCltn(): ?int
    {
        return $this->lamaCltn;
    }

    public function setLamaCltn(?int $lamaCltn): self
    {
        $this->lamaCltn = $lamaCltn;

        return $this;
    }
}
