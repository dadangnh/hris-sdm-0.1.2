<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\JnsCutiRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    security: 'is_granted(\'ROLE_USER\')'
)]
#[ORM\Entity(
    repositoryClass: JnsCutiRepository::class
)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(
    name: 't_jns_cuti'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama_cuti',
        'jns_cuti'
    ],
    name: 'idx_jns_cuti'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'namaCuti' => 'ipartial'
    ]
)]
#[ApiFilter(
    NumericFilter::class,
    properties: [
        'jnsCuti'
    ]
)]
class JnsCuti
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(
        type: 'integer'
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'permohonanCuti:read',
            'riwayatCuti:read',
        ]
    )]
    private ?int $id;

    #[ORM\Column(
        type: 'string',
        length: 75
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'permohonanCuti:read',
            'riwayatCuti:read',
        ]
    )]
    private ?string $namaCuti;

    #[ORM\Column(
        type: 'integer'
    )]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'permohonanCuti:read',
            'riwayatCuti:read',
        ]
    )]
    private ?int $jnsCuti;

    #[ORM\OneToMany(
        mappedBy: 'jenisCuti',
        targetEntity: JamKerjaCuti::class
    )]
    private Collection $jamKerjaCuti;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'permohonanCuti:read',
            'riwayatCuti:read',
        ]
    )]
    private $tglAktif;

    #[ORM\Column(type: 'date', nullable: true)]
    #[Groups(
        groups: [
            'jamkerjacuti:read',
            'permohonanCuti:read',
            'riwayatCuti:read',
        ]
    )]
    private $tglEnd;

    public function __construct()
    {
        $this->jamKerjaCuti = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNamaCuti(): ?string
    {
        return $this->namaCuti;
    }

    public function setNamaCuti(string $namaCuti): self
    {
        $this->namaCuti = $namaCuti;

        return $this;
    }

    public function getJnsCuti(): ?int
    {
        return $this->jnsCuti;
    }

    public function setJnsCuti(int $jnsCuti): self
    {
        $this->jnsCuti = $jnsCuti;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getJamKerjaCuti(): Collection
    {
        return $this->jamKerjaCuti;
    }

    public function addJamKerjaCuti(JamKerjaCuti $jamKerjaCuti): self
    {
        if (!$this->jamKerjaCuti->contains($jamKerjaCuti)) {
            $this->jamKerjaCuti[] = $jamKerjaCuti;
            $jamKerjaCuti->setJenisCuti($this);
        }

        return $this;
    }

    public function removeJamKerjaCuti(JamKerjaCuti $jamKerjaCuti): self
    {
        if ($this->jamKerjaCuti->removeElement($jamKerjaCuti)) {
            // set the owning side to null (unless already changed)
            if ($jamKerjaCuti->getJenisCuti() === $this) {
                $jamKerjaCuti->setJenisCuti(null);
            }
        }

        return $this;
    }

    public function getTglAktif(): ?\DateTimeInterface
    {
        return $this->tglAktif;
    }

    public function setTglAktif(?\DateTimeInterface $tglAktif): self
    {
        $this->tglAktif = $tglAktif;

        return $this;
    }

    public function getTglEnd(): ?\DateTimeInterface
    {
        return $this->tglEnd;
    }

    public function setTglEnd(?\DateTimeInterface $tglEnd): self
    {
        $this->tglEnd = $tglEnd;

        return $this;
    }
}
