<?php

namespace App\Controller;

use App\Entity\KantorUk3tsp;
use App\Entity\Uk3tsp;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Uk3tspController extends AbstractController
{
    /**
     * @throws JsonException
     * @throws Exception
     *
     */
    #[Route('/uk3tsps/permohonan_full', name: 'app_uk3tsp_permohonan_full', methods: ['POST'])]
    public function postPermohonanUk3tsp(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $requestData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $requestUk3tspData = $requestData['uk3tsp'];
        $requestKantorData = $requestData['kantor'];
        $kantor = null;

        // do validation
        if (null === $requestUk3tspData) {
            return $this->json([
                'code' => 401,
                'message' => 'Cannot proceed empty data.'
            ]);
        }

        // Save the variable to entity
        $uk3tsp = new Uk3tsp();
        $uk3tsp->setPmk($requestUk3tspData['pmk']);
        $uk3tsp->settanggal(new DateTime($requestUk3tspData['tanggal']));
        $uk3tsp->setKeterangan($requestUk3tspData['keterangan']);
        $uk3tsp->setStatus($requestUk3tspData['status']);
        $doctrine->getManager()->persist($uk3tsp);

        // Save the variable to entity
        foreach ($requestKantorData as $key=>$val){
            $kantor = new KantorUk3tsp();
            $kantor->setUsulanKantor($uk3tsp);
            $kantor->setNama($val['nama']);
            $kantor->setKantorId($val['kantorId']);
            $doctrine->getManager()->persist($kantor);
        }

        // Save to database
        $doctrine->getManager()->flush();

        return $this->json([
            'code' => 201,
            'message' => 'Data successfully created.',
            'uk3tsp' => $uk3tsp,
            'kantor' => $kantor
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws JsonException
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    #[Route('/uk3tsps/permohonan_full/{id}', name: 'app_uk3tsp_permohonan_full_update', methods: ['PATCH'])]
    public function patchPermohonanUk3tsp(ManagerRegistry $doctrine, Request $request, string $id): JsonResponse
    {
        $requestData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $requestUk3tspData = $requestData['uk3tsp'];
        $requestKantorData = $requestData['kantor'];

        /** @var Uk3tsp $uk3tspEdit */
        $uk3tspEdit = $doctrine
            ->getRepository(Uk3tsp::class)
            ->findPermohonanById($id);

        // do validation
        if (null === $uk3tspEdit) {
            return $this->json([
                'code' => 404,
                'message' => 'No data associated with this id'
            ]);
        }

        // Save the variable to entity
        $uk3tspEdit->setPmk($requestUk3tspData['pmk']);
        $uk3tspEdit->settanggal(new DateTime($requestUk3tspData['tanggal']));
        $uk3tspEdit->setKeterangan($requestUk3tspData['keterangan']);
        $uk3tspEdit->setStatus($requestUk3tspData['status']);
        $doctrine->getManager()->persist($uk3tspEdit);

        $kantorIdOnRequest = $kantorIdOnDatabase = [];

        foreach ($requestKantorData as $key => $val){
            $kantorIdOnRequest[] = $val['kantorId'];
        }
        $kantors = $uk3tspEdit->getKantor();

        foreach ($kantors as $kantor) {
            if (!in_array($kantor->getKantorId(), $kantorIdOnRequest, true)) {
                $doctrine->getManager()->remove($kantor);
            } else {
                $kantorIdOnDatabase[] = $kantor->getKantorId();
            }
        }

        foreach ($requestKantorData as $kantorData) {
            if (in_array($kantorData['kantorId'], $kantorIdOnDatabase, true)) {
                continue;
            }
            $kantor = new KantorUk3tsp();
            $kantor->setUsulanKantor($uk3tspEdit);
            $kantor->setNama($kantorData['nama']);
            $kantor->setKantorId($kantorData['kantorId']);
            $doctrine->getManager()->persist($kantor);
        }

        // Save to database
        $doctrine->getManager()->flush();
        $uk3tspNew = $doctrine
            ->getRepository(Uk3tsp::class)
            ->findPermohonanById($id);
        $kantors = $uk3tspNew->getKantor();

        return $this->json([
            'code' => 201,
            'message' => 'Data successfully Updated.',
            'uk3tsp' => $uk3tspEdit,
            'kantors' => $kantors
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    #[Route('/uk3tsps/active/pmk', methods: ['GET'])]
    public function getActivePmk(ManagerRegistry $doctrine): JsonResponse
    {
        $uk3tsps = $doctrine
            ->getRepository(Uk3tsp::class)
            ->findActivePmk();

        return $this->json([
            'uk3tsp' => $uk3tsps,
        ]);
    }
}
