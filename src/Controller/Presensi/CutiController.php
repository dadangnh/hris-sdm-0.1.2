<?php

namespace App\Controller\Presensi;

use App\Entity\CutiPegawai;
use App\Entity\CutiPegawaiStatus;
use App\Entity\PermohonanCuti;
use App\Entity\RekapCuti;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Uid\Uuid;

#[IsGranted("ROLE_USER")]
class CutiController extends AbstractController
{
    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|JsonException
     */
    #[Route('/rekap_cutis/cuti_diambil', methods: ['POST'])]
    public function showCutiDiambil(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content        = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $pegawaiId      = $content['pegawaiId'];
        $year           = $content['year'];
        $kota           = $content['kota'] ?? '';
        $provinsi       = $content['provinsi'] ?? '';
        $agama          = $content['agama'] ?? '';
        $tanggalAcuan   = $content['tanggalAcuan'] ?? '';

        if (!Uuid::isValid($pegawaiId)) {
            return $this->json([
                'code' => 404,
                'errors' => 'Please provide a valid uuid.'
            ]);
        }

        return $this->json($this->hitungSisa($doctrine, $pegawaiId, $year, $tanggalAcuan, $provinsi, $kota, $agama));
    }


    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws JsonException
     * @throws NotFoundExceptionInterface
     */
    #[Route('/rekap_cutis/sisa_cuti', methods: ['POST'])]
    public function getSisa(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content        = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $pegawaiId      = $content['pegawaiId'];
        $tahunIni       = $content['year'];

        $kota           = $content['kota'] ?? '';
        $provinsi       = $content['provinsi'] ?? '';
        $agama          = $content['agama'] ?? '';
        $tanggalAcuan   = $content['tanggalAcuan'] ?? '';

        if (!Uuid::isValid($pegawaiId)) {
            return $this->json([
                'code' => 404,
                'errors' => 'Please provide a valid uuid.'
            ]);
        }

        $cutiTahunIni  = $this->hitungSisa($doctrine, $pegawaiId, $tahunIni, $tanggalAcuan, $provinsi, $kota, $agama);
        $cutiTahunMin1 = $this->hitungSisa($doctrine, $pegawaiId, $tahunIni-1, $tanggalAcuan, $provinsi, $kota, $agama);
        $cutiTahunMin2 = $this->hitungSisa($doctrine, $pegawaiId, $tahunIni-2, $tanggalAcuan, $provinsi, $kota, $agama);

        $sisaCutiMin1 = $doctrine
            ->getRepository(RekapCuti::class)
            ->findOneBy([
                'pegawaiId'  => $pegawaiId,
                'tahun'      => ($tahunIni-1)
            ]);

        $sisaCutiMin2 = $doctrine
            ->getRepository(RekapCuti::class)
            ->findOneBy([
                'pegawaiId'  => $pegawaiId,
                'tahun'      => ($tahunIni-2)
            ]);

        $cutiPegawaiStatus = $doctrine
            ->getRepository(CutiPegawaiStatus::class)
            ->findOneBy([
                'pegawaiId'  => $pegawaiId
            ]);

        $n_2        = ($sisaCutiMin2) ? $sisaCutiMin2->getSisaCuti() : 0;
        $ct_n2      = $cutiTahunMin2['CutiTahunanDipakai'];;
        $n_1        = ($sisaCutiMin1) ? $sisaCutiMin1->getSisaCuti() : 0;
        $ct_n1      = $cutiTahunMin1['CutiTahunanDipakai'];;
        $ct_n       = $cutiTahunIni['CutiTahunanDipakai'];
        $hak_cuti   = ($cutiPegawaiStatus) ? (int) $cutiPegawaiStatus->getHakCuti() : 0;

        $cuti_bersama = 0;
        if ((0 === $ct_n2 || empty($ct_n2)) && (0 === $ct_n1 || empty($ct_n1)) && $n_2 >= 12) {
            $n2=6;
        } else {
            $n2=0;
        }
        if ((0 === $ct_n1 && 0 === $ct_n2) && $n_2 >= 12) {
            $n1=6;
        } else {
            $n1=(int) $n_1;
        }

        $hit = $n2 - (int) $ct_n;
        if ($hak_cuti > 0) {
            if (($hit) >= 0) {
                $n2 = $hit;
                $n = $hak_cuti - $cuti_bersama;
            } else if ($n2 <= 0) {
                $hit2 = $n1-abs($hit);
                $n2 = 0;
                if (($hit2) >= 0) {
                    $n1 = $hit2;
                    $n = $hak_cuti - $cuti_bersama;
                } else {
                    $n1 = 0;
                    $n = $hak_cuti - $cuti_bersama - abs($hit2);
                }
            }
            $cek_cb = $doctrine
                    ->getRepository(CutiPegawai::class)
                    ->count([
                       'pegawaiId'  => $pegawaiId,
                        'tahun'     => $tahunIni,
                        'jnsCuti'   => 1
                    ]);
            if($cek_cb>=1){
                $n=0;
            }
        }else {
            $n2 = 0;
            $n1 = 0;
            $n = 0;
        }

        return $this->json([
            'hakCuti'           => $hak_cuti,
            'kuotaTahunLalu2'   => $n2,
            'kuotaTahunLalu'    => $n1,
            'kuotaTahunIni'     => $n,
            'n_2'               => $n_2,
            'n_1'               => $n_1,
            'ct_n2'             => $ct_n2,
            'ct_n1'             => $ct_n1,
            'ct_n'              => $ct_n
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @throws Exception
     */
    #[Route('/rekap_cutis/lama_cuti', methods: ['POST'])]
    public function calcLamaCuti(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content        = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $tanggalMulai   = $content['tanggalMulai'];
        $tanggalSelesai = $content['tanggalSelesai'];

        $kota = $content['kota'] ?? '';
        $provinsi = $content['provinsi'] ?? '';
        $agama = $content['agama'] ?? '';

        $liburs = $this->getHariLibur($doctrine, $provinsi, $kota, $agama);

        $lamaCuti = $this->countWorkingDays($liburs, $tanggalMulai, $tanggalSelesai);

        return $this->json([
            'lamaHariKerja' => $lamaCuti
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @throws Exception
     */
    #[Route('/rekap_cutis/lama_cuti_clt', methods: ['POST'])]
    public function calcLamaCutiCLT(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content        = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $tanggalMulai   = $content['tanggalMulai'];
        $tanggalSelesai = $content['tanggalSelesai'];

        $tahunMulai     = substr($tanggalMulai,0,4) ?? 0;
        $tahunSelesai   = substr($tanggalSelesai,0,4) ?? 0;

        $kota       = $content['kota'] ?? '';
        $provinsi   = $content['provinsi'] ?? '';
        $agama      = $content['agama'] ?? '';

        $cutiDiambil = $cutiLT = 0;

        $liburs     = $this->getHariLibur($doctrine, $provinsi, $kota, $agama);

        $lamaCuti   = $this->countWorkingDays($liburs, $tanggalMulai, $tanggalSelesai);
        if($tahunMulai == $tahunSelesai && $tahunSelesai == date('Y')){
            $cutiDiambil    = $lamaCuti;
        } else {
            $cutiDiambil    = $this->countWorkingDays($liburs, $tanggalMulai, $tahunMulai.'-12-31');
            $cutiLT         = $this->countWorkingDays($liburs, $tahunSelesai.'-01-01', $tanggalSelesai);;
        }

        return $this->json([
            'lamaHariKerja' => $lamaCuti,
            'cutiDiambil'   => $cutiDiambil,
            'cutiLT'        => $cutiLT,
            'tanggalMulai'  => $tanggalMulai,
            'tanggalSelesai'=> $tanggalSelesai
        ]);
    }

    /**
     * @param $doctrine
     * @param $pegawaiId
     * @param $year
     * @param $tanggalAcuan
     * @param $provinsi
     * @param $kota
     * @param $agama
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    #[ArrayShape([
        'pegawaiId' => Uuid::class,
        'CutiTahunanDipakai' => "int|mixed"
    ])]
    private function hitungSisa($doctrine, $pegawaiId, $year, $tanggalAcuan, $provinsi, $kota, $agama): array
    {
        // Define the variables
        $tahunIni       = $year;
        $tahunLalu      = $tahunIni - 1;

        $pegawaiId = Uuid::fromString($pegawaiId);

        $cutis = $doctrine
            ->getRepository(CutiPegawai::class)
            ->findSummaryCutiByPegawaiIdAndTahun($pegawaiId, $year, $tanggalAcuan);

        $token = $this->container->get('security.token_storage')->getToken();

        $cutiLTBatalLTPD = $doctrine
            ->getRepository(CutiPegawai::class)
            ->findCutiLintasTahunPanggilanDinas($pegawaiId, $year, $tanggalAcuan);

        //regenerate array hari libur
        $arrayHariLiburs = $this->getHariLibur($doctrine, $provinsi, $kota, $agama);

        $cutiTahunanBatalAK = 0;
        $cutiTahunanBatalAW = 0;
        //Untuk menghitung cuti batal lintas tahun Panggilan Dinas
        foreach($cutiLTBatalLTPD as $key => $val) {
            $tahunBaseOnTglMulai = explode('-', $val['tgl_awal_cuti_peg']);
            $tahunBaseOnTglMulaiST = explode('-', $val['tanggal_mulai_tugas']);

            //get cuti batal akhir tahun
            if($tahunIni === $tahunBaseOnTglMulai[0]
                && $tahunIni === $tahunBaseOnTglMulaiST[0]
                && $tahunIni === $val['tahun']
            ) {
                $cutiTahunanBatalAK +=
                    $this->countWorkingDays($arrayHariLiburs, $val['tanggal_mulai_tugas'], $tahunIni . '-12-31');
            }

            //get cuti batal awal tahun
            if ($tahunLalu === $tahunBaseOnTglMulaiST[0]
                && $tahunIni === $val['tahun_lt']
            ) {
                $cutiTahunanBatalAW += $val['jumlah_cuti_lt'];
            }

            if ($tahunIni === $val['tahun_lt'] && $tahunIni === $tahunBaseOnTglMulaiST[0]){
                $cutiTahunanBatalAW +=
                    $this->countWorkingDays($arrayHariLiburs, $val['tanggal_mulai_tugas'], $val['tgl_akhir_cuti_peg']);
            }
        }

        $cutiBatalLT = $cutiTahunanBatalAK + $cutiTahunanBatalAW;

        $cutiTahunanDipakai = $cutis['cuti_tahunan'] + $cutis['cuti_tahunan_ak']
            + $cutis['cuti_tahunan_aw'] - $cutiBatalLT + $cutis['cuti_tahunan_setengah'];

        return [
            'pegawaiId'         => $pegawaiId,
            'CutiTahunanDipakai'=> $cutiTahunanDipakai
        ];
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     */
    #[Route('/permohonan_cutis/get_list_persetujuan_cuti', methods: ['POST'])]
    public function getListPersetujuanCuti(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content    = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $pegawaiId  = $content['pegawaiId'];

        $offset     = $content['offset'] ?? 0 ;
        $limit      = $content['limit'] ?? 1 ;

        $list       = $doctrine
                    ->getRepository(PermohonanCuti::class)
                    ->findListPersetujuanByPegawaiId($pegawaiId, $offset, $limit);

        $count      =$doctrine
                    ->getRepository(PermohonanCuti::class)
                    ->countListPersetujuanByPegawaiId($pegawaiId);

        return $this->json([
            "list"  => $list,
            "count" => $count
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     */
    #[Route('/hari_liburs/ambil_hari_libur', methods: ['POST'])]
    public function ambilHariLibur(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content    = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $kota       = $content['kota'] ?? '';
        $provinsi   = $content['provinsi'] ?? '';
        $agama      = $content['agama'] ?? '';

        $arrayHariLiburs = $this->getHariLibur($doctrine, $provinsi, $kota, $agama);

        return $this->json($arrayHariLiburs);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param String $provinsi
     * @param String $kota
     * @param String $agama
     * @return array
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    private function getHariLibur(ManagerRegistry $doctrine, String $provinsi, String $kota, String $agama): array
    {
        $liburs = $doctrine
            ->getRepository(CutiPegawai::class)
            ->findHariLibur($provinsi, $kota, $agama);

        //regenerate array hari libur
        $arrayHariLiburs = [];
        foreach($liburs as $key => $val){
            //$arrayHariLiburs[] = $val['tanggal'];
            $tglMulaiHitung     = new DateTime($val['tanggal_awal']);
            $tglSelesaiHitung   = new DateTime($val['tanggal_akhir']);

            for($i = $tglMulaiHitung; $i <= $tglSelesaiHitung; $i->modify('+1 day')){
                $date_cek = $i->format("Y-m-d");
                if (!in_array($date_cek, $arrayHariLiburs, true)
                    && !in_array($i->format('N'), array('6','7'))
                ) {
                    $arrayHariLiburs[] = $i->format('Y-m-d');
                }
            }
        }

        return $arrayHariLiburs;
    }

    /**
     * @param array $hariLibur
     * @param String $tanggalMulai
     * @param String $tanggalSelesai
     * @return int
     * @throws Exception
     */
    private function countWorkingDays(array $hariLibur,String $tanggalMulai,String $tanggalSelesai): int
    {
        $tglMulaiHitung     = new DateTime($tanggalMulai);
        $tglSelesaiHitung   = new DateTime($tanggalSelesai);

        $j = 0;
        for($i = $tglMulaiHitung; $i <= $tglSelesaiHitung; $i->modify('+1 day')){
            $date_cek = $i->format("Y-m-d");
            if (!in_array($date_cek, $hariLibur, true)
                && !in_array($i->format('N'), array('6','7'))
            ) {
                $j++;
            }
        }

        return $j;
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @throws \Doctrine\DBAL\Exception
     */
    #[Route('/permohonan_cutis/get_nomor', methods: ['POST'])]
    public function getNomorSurat(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content    = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $kodeNomor  = $content['kodeNomor'] ?? '';
        $tahunNomor = $content['tahunNomor'] ?? '';

        $liburs = $doctrine
            ->getRepository(PermohonanCuti::class)
            ->getMaxNomorSurat($kodeNomor,$tahunNomor);

        return $this->json($liburs[0]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param $permohonanId
     * @return JsonResponse|Response
     */
    #[Route('/cuti_pegawais/delete_by_permohonan/{permohonanId}', methods: ['DELETE'])]
    public function deleteRiwayatCutiByPermohonanId(ManagerRegistry $doctrine, $permohonanId)
    {
        if (!Uuid::isValid($permohonanId)) {
            return $this->json([
                'code' => 404,
                'errors' => 'Please provide a valid uuid.'
            ]);
        }

        $permohonan = $doctrine->getRepository(PermohonanCuti::class)
            ->find($permohonanId);

        $jnsCuti = $permohonan->getJnsCuti();

        if('9' == $jnsCuti->getJnsCuti()){
            $rekapCuti = $doctrine->getRepository(RekapCuti::class)
                ->findOneBy([
                    'pegawaiId'  => $permohonan->getPegawaiId(),
                    'tahun'      => $permohonan->getTahun()
                ]);

            if($rekapCuti){
                $em = $doctrine->getManager();
                $em->remove($rekapCuti);
                $em->flush();
            }
        }

        $doctrine->getRepository(CutiPegawai::class)
            ->deleteByPermohonanId($permohonanId);

        return new Response(null, 204);//204
    }
}
