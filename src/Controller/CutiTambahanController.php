<?php

namespace App\Controller;

use App\Entity\PermohonanCutiTambahan;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CutiTambahanController extends AbstractController
{
    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/permohonan_cuti_tambahans/get_list_persetujuan_cuti_tambahan', methods: ['POST'])]
    public function getListPersetujuanCutiTambahan(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content    = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $pegawaiId  = $content['pegawaiId'];

        $offset     = $content['offset'] ?? 0 ;
        $limit      = $content['limit'] ?? 1 ;

        $list       = $doctrine
            ->getRepository(PermohonanCutiTambahan::class)
            ->findListPersetujuanByPegawaiId($pegawaiId, $offset, $limit);

        $count      =$doctrine
            ->getRepository(PermohonanCutiTambahan::class)
            ->countListPersetujuanByPegawaiId($pegawaiId);

        return $this->json([
            "list"  => $list,
            "count" => $count
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/permohonan_cuti_tambahans/get_cuti_tambahan_dipakai', methods: ['POST'])]
    public function getCutiTambahanDiPakai(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $content    = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $pegawaiId  = $content['pegawaiId'];

        $cutiTambahanDipakai = $doctrine
            ->getRepository(PermohonanCutiTambahan::class)
            ->findCutiTambahanDipakai($pegawaiId);

        return $this->formatReturnData($cutiTambahanDipakai);
    }

    /**
     * @param array|null $cutiTambahanDipakai
     * @return JsonResponse
     */
    private function formatReturnData(?array $cutiTambahanDipakai): JsonResponse
    {
        $dataCutiDipakai = $cutiTambahanDipakai[0];
        if (null === $dataCutiDipakai['cutiTambahanDipakai']) {
            return $this->json([
                'cutiTambahanDipakai' => 0
            ], 200);
        }

        return $this->json([
            'cutiTambahanDipakai' => $dataCutiDipakai['cutiTambahanDipakai'],
        ]);
    }

}
