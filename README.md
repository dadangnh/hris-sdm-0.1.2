# Backend HRIS-SDM-0.1.2

[![pipeline status](https://gitlab.com/prasasta/hris-sdm-0.1.2/badges/master/pipeline.svg)](https://gitlab.com/prasasta/hris-sdm-0.1.2/-/commits/master)
[![coverage report](https://gitlab.com/prasasta/hris-sdm-0.1.2/badges/master/coverage.svg)](https://gitlab.com/prasasta/hris-sdm-0.1.2/-/commits/master)

![Lines of code](https://img.shields.io/tokei/lines/gitlab.com/prasasta/hris-sdm-0.1.2)
![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/prasasta/hris-sdm-0.1.2)

Source code Backend HRIS-SDM-0.1.2.

## Canonical source

The canonical source of Backend HRIS-SDM-0.1.2 where all development takes place is [hosted on GitLab.com](https://gitlab.com/prasasta/hris-sdm-0.1.2).

## Installation

Please check [INSTALLATION.md](INSTALLATION.md) for the installation instruction.

## Contributing

This is an open source project, and we are very happy to accept community contributions.

# License

This code is published under [GPLv3 License](LICENSE).
