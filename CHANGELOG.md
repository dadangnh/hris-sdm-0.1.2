# Backend HRIS-SDM-0.1.2 Changelog

## Version 1.5.2
  * fix pipeline to deploy to docker registry

## Version 1.5.1
  * hot fix symfony 6.2.3

## Version 1.5.0
  * Update infrastructure configuration from upstream (#30)
  * Menghilangkan Deprecation message Api-Platform (#31)
  * Upgrade Api Platform to v3.0.8 (#32)
  * Upgrade Symfony Version to v6.2 (#33)
  * Upgrade PHP 8.2 dan Postgresql v15 (#34)

## Version 1.4.1
  * Updated Symfony Components and Dependencies (#28)
  * Fix Caddy failed to build (#29)

## Version 1.4.0
  * Updated Docker and Infrastructure Configuration (#27)
  * Updated Symfony Components and Dependencies
  * Updated Symfony Recipes

## Version 1.3.7
  * Add new event listener to handle new entity creation (#24)
  * Fix failed test job on symfony cli and gitlab-ci services (#25)
  * Fix failed job from updated docker-compose standard (#26)
  * Updated Symfony Components and Dependencies
  * Updated Symfony Recipes

## Version 1.3.6
  * Expose more filter (#21)
  * Add grouping on Cuti Entity (#22)
  * Add more attributes on PermohonanCutiTambahan (#23)
  * Updated dependencies

## Version 1.3.5
  * Added more information and filter on Cuti Endpoint (#20)
  * Updated dependencies

## Version 1.3.4
  * Fixed a bug on PermohonanCuti (#19)
  * Updated components, dependencies, and recipes

## Version 1.3.3
  * Added a new column on jenisCuti table (#17)
  * Fixed a bug on cutiPegawai repository (#18)

## Version 1.3.2
  * Added endpoint CutiTambahan and JenisCutiTambahan (#13)(#15)
  * Update GitLab CI template (#14)
  * Updated components and dependencies (#16)

## Version 1.3.1
  * Fix bug error data type (#12)

## Version 1.3.0
  * Updated Symfony Version to 6.1 (#11)

## Version 1.2.1
  * Added new event listener
  * Updated components and dependencies

## Version 1.2.0
  * Added new endpoints
  * Updated components and dependencies

## Version 1.1.1
  * Update infrastructure configuration (#8)
  * Added new endpoints
  * Updated components and dependencies

## Version 1.1.0
  * Upgrade Symfony to v6 (#7)
  * Updated components and dependencies
  * Added new endpoints

## Version 1.0.3
  * Added new endpoints 
  * Updated components and dependencies

## Version 1.0.2
  * Added new endpoints
  * Updated components and dependencies

## Version 1.0.1
  * Updated components and dependencies
  * Updated infrastructure configuration
  * Added more endpoint

## Version 1.0.0
  * Released based on Symfony 5.4

## Version 1.0.0-rc.3
  * Updated components and dependencies

## Version 1.0.0-rc.2
  * Expose pagination configuration to client on endpoint
  * Updated components and dependencies

## Version 1.0.0-rc.1
  * Implement groupings on Entity
  * Added Filters on endpoint (#4)
  * Updated dependencies

## Version 1.0.0-Beta3
  * Implement new version of SharedAuthLibrary

## Version 1.0.0-Beta2
  * Added grouping on endpoint
  * Updated components, dependencies, and recipes

## Version 1.0.0-Beta
  * First Beta release. Contains basic working code for initial Backend SDM-0.1.2 workload
